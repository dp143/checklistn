package com.dp.mychecklist.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dp.mychecklist.roomDB.DBThread;

/**
 * Created by pramodpatil305 on 15-02-2018.
 */

public class SessionManager {

    public static final String MOBILE_NO = "mobile_no";
    public static final String OTP = "otp";
    public static final String USER_NAME = "userName";
    public static final String USER_ID = "userID";
    public static final String USER_EMAIL= "email";
    public static final String ACCESS_TOKEN = "token";
    public static final String PASSWORD = "password";

    SharedPreferences preferences;
    Context context;

    public SessionManager(Context context) {
        this.context = context;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getStringValue(String key) {
        Log.i("SessionManager", key + "-" + preferences.getString(key, " ") + " Retrieve");
        return preferences.getString(key, "");
    }

    public void setStringValue(String key, String value) {
        try {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(key, value);
            editor.commit();
            editor.apply();
            Log.i("SessionManager", key + "-" + value + " Saved");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearData()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                new DBThread(context).deleteDB();
            }
        }).start();

        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        editor.apply();
    }
}
