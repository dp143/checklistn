package com.dp.mychecklist.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.roomDB.DBThread;

import org.json.JSONArray;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

public class Utility
{
    static ProgressDialog progressDialog;

    public static final String DATE_FORMAT = "yyyyMMddhhmmss";

    public static final String BASE_URL  = "http://192.168.43.232/api/";//share event with m
    public static final String URL_LOGIN = BASE_URL+"auth/login";
    public static final String URL_REGISTER = BASE_URL+"auth/register";
    public static final String URL_CHANGE_PASSWORD = BASE_URL+"auth/setPassword";
    public static final String URL_LOGOUT = BASE_URL+"auth/logout";
    public static final String URL_ADD_EVENT = BASE_URL+"events/setEvents";
    public static final String URL_DELETE_EVENT = BASE_URL+"events/deleteEvent";
    public static final String URL_DELETE_TASK = BASE_URL+"events/deleteTask";
    public static final String URL_ADD_TASK = BASE_URL+"events/setTasks";
    public static final String URL_GET_EVENT_TASK_DETAILS = BASE_URL+"events/getEventTasksDetails";
    public static final String URL_GET_EVENT_LIST = BASE_URL+"events/getEventList";
    public static final String URL_SHARE_EVENT = BASE_URL+"events/setShareEvents";
    public static final String MSG_PERMISSION = "Please allow all permissions to use the app features...";

    public static void hideKeyboard(Context context, View view)
    {
        try
        {
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void showInternetDialog(Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Please connect to working internet connection and tray again");
        builder.setTitle("No Internet");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        Dialog dialog = builder.create();
        dialog.show();
    }

    public static void showShareAppDialog(final Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("The selected user is not registered with our app, Please share the app with the user");
        builder.setTitle("Share App");
        builder.setPositiveButton("Share", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                shareApp(context);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        Dialog dialog = builder.create();
        dialog.show();
    }

    public static void showProgressDialog(Context context)
    {
        try {
            if (progressDialog == null)
                progressDialog = new ProgressDialog(context);

            if (!progressDialog.isShowing()) {
                progressDialog.setMessage("Please wait...");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void dismissDialog() {
        if (progressDialog != null && progressDialog.isShowing())
        {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public static String setEventDateinFormat(String date)
    {
        Calendar calendar = getCalendarInstance(date);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
        String finalDate = simpleDateFormat.format(calendar.getTime());

        Log.d("Formatted Date", finalDate);
        return finalDate;
    }

    public static Calendar getCalendarInstance(String eventDate)
    {
        try {
            Calendar calendar = Calendar.getInstance();

            String[] strDateTime = eventDate.split(" ");
            int year = Integer.parseInt(strDateTime[0].split("-")[0]);
            int month = Integer.parseInt(strDateTime[0].split("-")[1]);
            int day = Integer.parseInt(strDateTime[0].split("-")[2]);

            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month - 1);
            calendar.set(Calendar.DAY_OF_MONTH, day);

            return calendar;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean comapreWithTodaysDate(String strDate) {
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        String todaysDate = formatter.format(today);
        Log.d("Todays date", todaysDate);
        Log.d("Task date", strDate);

        try {
            Date todays = formatter.parse(todaysDate);

            Date selected = formatter.parse(strDate);

            Log.d("Date", selected.compareTo(todays) + "");

            if (todays.compareTo(selected) > 0)
                return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public static String getCurrentDate()
    {
        try {
            SimpleDateFormat formatterPass = new SimpleDateFormat(DATE_FORMAT);
            Date dateobjCurrentTime = new Date();
            String strCurrentTime = formatterPass.format(dateobjCurrentTime);
            Log.d("Current Date",strCurrentTime);
            return strCurrentTime;
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDateInDDMMYYYYHHMM(String date)
    {
        int month,year,day;
        String time;

        year = Integer.parseInt(date.substring(0,4));
        month = Integer.parseInt(date.substring(4,6));
        day = Integer.parseInt(date.substring(6,8));

        String strDate,strMonth;
        if(month < 10)
            strMonth = "0"+month;
        else
            strMonth = month+"";

        if(day < 10)
            strDate = "0"+day;
        else
            strDate = day+"";

        return strDate+"-"+strMonth+"-"+year;
    }

    public void showOTPNotification(Context context, String otp)
    {
        String CHANNEL_ID = "my_channel_01";
        CharSequence name = "Checklog";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;

        String msg = "OTP is : "+otp;
        NotificationCompat.Builder n = new NotificationCompat.Builder(context)
                .setContentTitle("CheckList")
                .setContentText(msg)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setVibrate(new long[] { 1000, 1000})
                .setLights(Color.RED, 3000, 3000)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setChannelId(CHANNEL_ID);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
            mChannel.setShowBadge(true);
        }

        notificationManager.notify(0, n.build());
    }

    public static String getDateInDDMMYYYYHHMMSS(String date,boolean isCurrentFromat)
    {
        try {
            DateFormat originalFormat = new SimpleDateFormat("yyyyMMddhhmmss", Locale.ENGLISH);
            DateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss ");
            String formattedDate;

            if(isCurrentFromat) {
                Date dates = targetFormat.parse(date);
                formattedDate = originalFormat.format(dates);
            }
            else{
                Date dates = originalFormat.parse(date);
                formattedDate = targetFormat.format(dates);
            }
            return formattedDate;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFormatted(int day)
    {
        String strHour = day+"";
        if(strHour.length() == 1)
            strHour = "0"+day;

        return strHour;
    }
    public static SortedMap<String, String> getContactsIntoArrayList(Context context, boolean allList)
    {
        JSONArray jsonArray = new JSONArray();
        ArrayList<ContactModel> contactModelArrayList = new ArrayList<>();
        Set<ContactModel> contactModelSet = new HashSet<>();
        Cursor cursor;

        if(allList)
            cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null, null, null);
        else
            cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null, null, null);

        String name, phonenumber;
        Set<String> listPhone = new TreeSet<>();

        SortedMap<String,String> hashMapContacts = new TreeMap<>();

        while (cursor.moveToNext())
        {
            ContactModel contactModel = new ContactModel();
            name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            phonenumber = phonenumber.replace(" ","");
            phonenumber = phonenumber.replace("-","");

            Log.d("Initial",phonenumber);

            if(phonenumber.length()>= 10) {
                if (phonenumber.length() > 10)
                    phonenumber = phonenumber.substring(phonenumber.length() - 10);

                Log.d("After", phonenumber);
                listPhone.add(phonenumber);
                contactModel.setName(name);
                contactModel.setContactNumber(phonenumber);
                contactModelSet.add(contactModel);

                hashMapContacts.put(name,phonenumber);
            }
        }

        return hashMapContacts;
      /*Commented for displaying the app contacts

        for(String phone:listPhone) {
            try {
                if(phone.length()>=10)
                    jsonArray.put(Long.parseLong(phone));
            } catch (Exception e)
            {
                e.printStackTrace();
            }
            // Log.d("Number",jsonArray.toString());
        }
        cursor.close();

        if(allList)
            return jsonArray;
        else
        {
            contactModelArrayList = new ArrayList<>(contactModelSet);
            return contactModelArrayList;
        }*/
    }

    public static void shareApp(Context context)
    {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Download Checklist App from this below link\n"+"https://drive.google.com/open?id=19ldTiANcGCSb0hTPALU7pl1jzraSS5-h");
        sharingIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sharingIntent, "Invite a friend"));
    }

    public static boolean isValid(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    public static boolean isValidPassword(String password)
    {
        String emailRegex =  "((?=.*\\d)(?=.*[a-zA-Z]).{8,20})";;

        Pattern pat = Pattern.compile(emailRegex);
        if (password == null)
            return false;
        return pat.matcher(password).matches();
    }
}
