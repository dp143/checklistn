package com.dp.mychecklist.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.fragments.FragmentLogin;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Apple on 10/01/18.
 */

public class APICall
{
    public static String TAG = APICall.class.getSimpleName();
    static RequestQueue mRequestQueue = null;

    public interface APIListener
    {
        void onSuccess(Object object);
        void onError(Object object);
    }

    public static void makeCall(final Map<String,String> params, String urls, final Context context, int method,
                                final boolean isHeader, boolean isShow, final APIListener listener)
    {
        try {
          if (ConnectivityReceiver.isConnected())
            {
                if(isShow)
                    Utility.showProgressDialog(context);

                makeURL(urls,params);

                if(mRequestQueue == null)
                    mRequestQueue = Volley.newRequestQueue(context);//.getInstance(context).getRequestQueue();//JacksonNetwork.newRequestQueue(context);
                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(method, urls,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response)
                            {
                                Utility.dismissDialog();
                                listener.onSuccess(response);
                                Log.d("Success", response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Utility.dismissDialog();
                        if(error instanceof TimeoutError)
                            Toast.makeText(context,"slow connection, please try again later",Toast.LENGTH_LONG).show();
                      /*  else if(error instanceof ServerError)
                        {
                            Toast.makeText(context,"Session expired, please login again.",Toast.LENGTH_LONG).show();
                            new SessionManager(context).clearData();
                            ((MainActivity) context).callFragment(new FragmentLogin());
                        }*/
                        else
                            listener.onError(error);
//                        Log.d("Error", error + "");
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String> params = new HashMap<String, String>();
                        if(isHeader)
                            params.put("X-ACCESS-TOKEN", new SessionManager(context).getStringValue(SessionManager.ACCESS_TOKEN));
                        //params.put("Accept-Language", "fr");

                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        5000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                // Add the request to the RequestQueue.
                mRequestQueue.add(stringRequest);

          } else {
                Toast.makeText(context, "Please connect to working internet connection!", Toast.LENGTH_SHORT).show();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public static void makeCallService(final Map<String,String> params, String urls, final Context context, int method,
                                final boolean isHeader, boolean isShow, final APIListener listener)
    {
        try {
          if (ConnectivityReceiver.isConnected())
            {
                if(isShow)
                    Utility.showProgressDialog(context);

                makeURL(urls,params);

                if(mRequestQueue == null)
                    mRequestQueue = Volley.newRequestQueue(context);//.getInstance(context).getRequestQueue();//JacksonNetwork.newRequestQueue(context);
                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(method, urls,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response)
                            {
                                Utility.dismissDialog();
                                listener.onSuccess(response);
                                Log.d("Success", response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        /*Utility.dismissDialog();
                        if(error instanceof TimeoutError)
                            Toast.makeText(context,"slow connection, please try again later",Toast.LENGTH_LONG).show();
                         else
                            listener.onError(error);
            */            Log.d("Error", error + "");
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String> params = new HashMap<String, String>();
                        if(isHeader)
                            params.put("X-ACCESS-TOKEN", new SessionManager(context).getStringValue(SessionManager.ACCESS_TOKEN));
                        //params.put("Accept-Language", "fr");

                        return params;
                    }
                };
                // Add the request to the RequestQueue.
                mRequestQueue.add(stringRequest);

          } else {
               // Toast.makeText(context, "Please connect to working internet connection!", Toast.LENGTH_SHORT).show();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void makeURL(String url, Map<String,String> mapData)
    {
        Log.e("URL",url);

        if(mapData!=null)
        {
            for(Map.Entry<String,String> entrySet : mapData.entrySet())
            {
                Log.e("Parameters",entrySet.getKey()+" : "+entrySet.getValue());
            }
        }
    }
}
