package com.dp.mychecklist.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.android.volley.Request;
import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.roomDB.DBThread;
import com.dp.mychecklist.roomDB.entities.TEvent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class GetSharedEvent  extends Service {

    public static final int notify = 1000*30*1;  //interval between two services(Here Service run every 5 Minute)
    private Handler mHandler = new Handler();   //run on another Thread to avoid crash
    private Timer mTimer = null;    //timer handling
    private String taskName = "";

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
     //   Toast.makeText(this, "Service is onCreate", Toast.LENGTH_SHORT).show();

        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
        else
            mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);   //Schedule task
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();    //For Cancel Timer
      //  Toast.makeText(this, "Service is Destroyed", Toast.LENGTH_SHORT).show();
    }

    //class TimeDisplay for handling task
    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    // display toast
                  //  Toast.makeText(GetSharedEvent.this, "Service is running", Toast.LENGTH_SHORT).show();
                    getAllEvents();
                }
            });
        }
    }

    public void getAllEvents()
    {
        APICall.makeCallService(null,Utility.URL_GET_EVENT_LIST, GetSharedEvent.this, Request.Method.POST, true, false,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object) {
                        try {
                            String result = object.toString();
                            final ArrayList<TEvent> tEventArrayList = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(result);

                            if(jsonObject.has("shared_event"))
                            {
                                JSONArray jsonArray1 = jsonObject.getJSONArray("shared_event");
                                for(int i=0;i<jsonArray1.length();i++)
                                {
                                    JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                    TEvent tEvent = new TEvent();
                                    tEvent.setEventId(jsonObject1.getString("id"));
                                    tEvent.setTaskTypeId(jsonObject1.getString("fk_taskTypeId"));
                                    tEvent.setEventDT(jsonObject1.getString("record_date"));
                                    tEvent.setEventSt("1");
                                    tEvent.setUserId(jsonObject1.getString("user_code"));
                                    tEvent.setEventName(jsonObject1.getString("event_title"));
                                    tEvent.setEventDescription(jsonObject1.getString("event_description"));
                                    tEvent.setEventDate(jsonObject1.getString("event_date"));
                                    tEvent.setEventRepeateDays(jsonObject1.getString("event_repeatDays"));
                                    tEventArrayList.add(tEvent);

                                 //   if(tEvent.getTaskTypeId().equals("1"))
                                   //     new GlobalAPIs(GetSharedEvent.this).getEventDetails(tEvent.getEventId(),null);
                                }
                            }

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    TEvent[] tEvents;
                                    ArrayList<TEvent> arrTemp = new ArrayList<>();

                                    for(int i=0;i<tEventArrayList.size();i++)
                                    {
                                        if(new DBThread(GetSharedEvent.this).getEventById(tEventArrayList.get(i).getEventId()) == null)
                                            arrTemp.add(tEventArrayList.get(i));
                                    }

                                    taskName = "";
                                    if(arrTemp.size()>0)
                                    {
                                        tEvents = new TEvent[arrTemp.size()];
                                        for(int i=0;i<arrTemp.size();i++) {
                                            tEvents[i] = arrTemp.get(i);
                                            taskName = taskName + arrTemp.get(i).getEventName();
                                            if(i<arrTemp.size()-1)
                                                taskName = taskName+", ";

                                        }
                                        new DBThread(GetSharedEvent.this).insertEvents(tEvents);
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable("EV",tEvents);
                                        Message message=  new Message();
                                        message.setData(bundle);
                                        handler.handleMessage(message);
                                    }
                                }
                            }).start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Object object) {
                    }
                });
    }

    Handler handler = new Handler(Looper.getMainLooper())
    {
        @Override
        public void handleMessage(Message msg) {
            TEvent[] tEvents = (TEvent[]) msg.getData().getSerializable("EV");
            for(int i=0;i<tEvents.length;i++)
            {
                new GlobalAPIs(GetSharedEvent.this).getEventDetails(tEvents[i].getEventId(),null);
            }

            showTaskShareNotification(GetSharedEvent.this);
        }
    };

    public void showTaskShareNotification(Context context)
    {
        String CHANNEL_ID = "my_channel_01";
        CharSequence name = "Checklist";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;

        String msg = "Event - " + taskName + " shared with you " ;

        Intent intent = new Intent(GetSharedEvent.this,MainActivity.class);
        PendingIntent pendIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationCompat.Builder n = new NotificationCompat.Builder(context)
                .setContentTitle("CheckList")
                .setContentText(msg)
                .setSmallIcon(R.drawable.app_logo_inside)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendIntent)
                .setVibrate(new long[]{1000, 1000})
                .setLights(Color.RED, 3000, 3000)
                .setChannelId(CHANNEL_ID);

        n.addAction(R.drawable.user, "OK", PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT));

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
            mChannel.setShowBadge(true);
        }

        notificationManager.notify(0, n.build());
    }

}