package com.dp.mychecklist.util;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.android.volley.Request;
import com.dp.mychecklist.fragments.FragmentLogin;
import com.dp.mychecklist.roomDB.DBThread;
import com.dp.mychecklist.roomDB.entities.TEvent;
import com.dp.mychecklist.roomDB.entities.TTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by pramodpatil305 on 23-03-2018.
 */

public class GlobalAPIs
{
    Context mContext;

    public GlobalAPIs(Context context)
    {
        this.mContext = context;
    }

    public void getAllEvents(final Fragment fragment)
    {
        APICall.makeCall(null,Utility.URL_GET_EVENT_LIST, mContext, Request.Method.POST, true, false,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object) {
                        try {
                            String result = object.toString();
                            ArrayList<TEvent> tEventArrayList = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray jsonArray = jsonObject.getJSONArray("event");
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                TEvent tEvent = new TEvent();
                                tEvent.setEventId(jsonObject1.getString("id"));
                                tEvent.setTaskTypeId(jsonObject1.getString("fk_taskTypeId"));
                                tEvent.setEventDT(jsonObject1.getString("record_date"));
                                tEvent.setEventSt("1");
                                tEvent.setUserId(jsonObject1.getString("user_code"));
                                tEvent.setEventName(jsonObject1.getString("event_title"));
                                tEvent.setEventDescription(jsonObject1.getString("event_description"));
                                tEvent.setEventDate(jsonObject1.getString("event_date"));
                                tEvent.setEventRepeateDays(jsonObject1.getString("event_repeatDays"));
                                tEventArrayList.add(tEvent);

                                if(tEvent.getTaskTypeId().equals("1"))
                                    getEventDetails(tEvent.getEventId(),fragment);
                            }

                            if(jsonObject.has("shared_event"))
                            {
                                JSONArray jsonArray1 = jsonObject.getJSONArray("event");
                                for(int i=0;i<jsonArray1.length();i++)
                                {
                                    JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                    TEvent tEvent = new TEvent();
                                    tEvent.setEventId(jsonObject1.getString("id"));
                                    tEvent.setTaskTypeId(jsonObject1.getString("fk_taskTypeId"));
                                    tEvent.setEventDT(jsonObject1.getString("record_date"));
                                    tEvent.setEventSt("1");
                                    tEvent.setUserId(jsonObject1.getString("user_code"));
                                    tEvent.setEventName(jsonObject1.getString("event_title"));
                                    tEvent.setDeleted(jsonObject1.getString("deleted"));
                                    tEvent.setEventDescription(jsonObject1.getString("event_description"));
                                    tEvent.setEventDate(jsonObject1.getString("event_date"));
                                    tEvent.setEventRepeateDays(jsonObject1.getString("event_repeatDays"));
                                    tEventArrayList.add(tEvent);

                                    if(tEvent.getTaskTypeId().equals("1"))
                                        getEventDetails(tEvent.getEventId(),fragment);
                                }
                            }

                            TEvent[] tEvents = new TEvent[tEventArrayList.size()];
                            for(int i=0;i<tEventArrayList.size();i++)
                            {
                                tEvents[i] = tEventArrayList.get(i);
                            }

                            new DBThread(mContext).insertEvents(tEvents);

                            Utility.dismissDialog();
                            ((FragmentLogin) fragment).syncDone();
                        } catch (Exception e) {
                            ((FragmentLogin) fragment).syncDone();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Object object) {
                        ((FragmentLogin) fragment).syncDone();
                    }
                });
    }

    public void getEventDetails(final String eventId,final Fragment fragment)
    {
        HashMap<String, String> params=new HashMap<>();
        params.put("txtEventCode",eventId);
        APICall.makeCall(params,Utility.URL_GET_EVENT_TASK_DETAILS, mContext, Request.Method.POST, true, false,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object) {
                        try {
                            String result = object.toString();
                            ArrayList<TTask> tTaskArrayList = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray jsonArray = jsonObject.getJSONArray("task");
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                TTask tTask = new TTask();
                                tTask.setTaskId(jsonObject1.getString("id"));
                                tTask.setTaskName(jsonObject1.getString("task_name"));
                                tTask.setFk_eventId(eventId);
                                tTask.setTaskSt("1");
                                tTask.setTaskDT(jsonObject1.getString("record_date"));
                                tTask.setTaskDescription(jsonObject1.getString("task_description"));
                                tTask.setTaskDate(jsonObject1.getString("task_date"));
                                tTask.setDeleted(jsonObject1.getString("deleted"));

                                tTaskArrayList.add(tTask);
                            }

                            TTask[] tTasks = new TTask[tTaskArrayList.size()];
                            for(int i=0;i<tTaskArrayList.size();i++)
                            {
                                tTasks[i] = tTaskArrayList.get(i);
                            }

                            new DBThread(mContext).insertTasks(tTasks);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Object object) {
                    }
                });
    }
}
