package com.dp.mychecklist;

import android.app.Application;
import android.os.Bundle;

/**
 * Created by pramodpatil305 on 05-04-2018.
 */

public class AppController extends Application
{
    static AppController mInstance;
    static Bundle bundle;

    @Override
    public void onCreate() {
        mInstance = this;
        super.onCreate();
    }

    public static Bundle getBundle() {
        return bundle;
    }

    public static void setBundle(Bundle bundle) {
        AppController.bundle = bundle;
    }

    public static synchronized AppController getInstance()
    {
        return mInstance;
    }

}
