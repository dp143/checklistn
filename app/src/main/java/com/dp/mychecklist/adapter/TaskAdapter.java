package com.dp.mychecklist.adapter;

/**
 * Created by pramodpatil305 on 09-02-2018.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.fragments.FragmentAddEvent;
import com.dp.mychecklist.fragments.FragmentAddTask;
import com.dp.mychecklist.fragments.FragmentContact;
import com.dp.mychecklist.fragments.FragmentEventList;
import com.dp.mychecklist.fragments.FragmentRecurringList;
import com.dp.mychecklist.fragments.FragmentRecurringReminder;
import com.dp.mychecklist.fragments.FragmentReminder;
import com.dp.mychecklist.fragments.FragmentReminderList;
import com.dp.mychecklist.roomDB.DBThread;
import com.dp.mychecklist.roomDB.entities.TEvent;
import com.dp.mychecklist.roomDB.entities.TTask;
import com.dp.mychecklist.util.APICall;
import com.dp.mychecklist.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Pramod on 21/12/17.
 */

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.MyViewHolder>
{
    ArrayList<TTask> commonModelArrayList;
    MyViewHolder myViewHolder;
    Fragment fragment;
    static Context context;
    String from;
    RecyclerView recyclerView;
    Activity activity;
    private Random mRandom = new Random();
    TEvent tEvent;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_item_new , parent, false);

        //View view = inflater.inflate(R.layout.sec_cat_templ_item,null);
        myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        try {
                final TTask model = commonModelArrayList.get(position);

           //     myViewHolder.imgShare.setVisibility(View.GONE);
                Log.d("Id",model.getTaskId());
                if(!model.getTaskId().equals("0")) {
                    myViewHolder.tvEvent.setText(model.getTaskName());
                    myViewHolder.tvDate.setText(Utility.getDateInDDMMYYYYHHMMSS(model.getTaskDate(), false));

                    myViewHolder.imgMenu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PopupMenu popup = new PopupMenu(context, v);
                            popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                            popup.getMenu().getItem(1).setVisible(false);

                            //registering popup with OnMenuItemClickListener
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                public boolean onMenuItemClick(MenuItem item) {
                                    if (item.getItemId() == R.id.menuEdit) {
                                        if (!commonModelArrayList.get(position).getTaskId().equals("0")) {
                                            Log.d("ID", commonModelArrayList.get(position).getTaskRecordId() + "");
                                            FragmentAddTask fragmentAddTask = new FragmentAddTask();
                                            Bundle bundle = new Bundle();
                                            bundle.putSerializable("TS", commonModelArrayList.get(position));
                                            bundle.putSerializable("EV", tEvent);
                                            fragmentAddTask.setArguments(bundle);
                                            ((MainActivity) context).callFragment(fragmentAddTask);
                                        }
                                    }
                                    else if (item.getItemId() == R.id.menuDelete) {
                                        showDeleteDialog(commonModelArrayList.get(position));
                                    }
                                    return true;
                                }
                            });
                            popup.show();//showing popup menu
                        }
                    });

                    myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!commonModelArrayList.get(position).getTaskId().equals("0")) {
                                Log.d("ID", commonModelArrayList.get(position).getTaskRecordId() + "");
                                FragmentAddTask fragmentAddTask = new FragmentAddTask();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("TS", commonModelArrayList.get(position));
                                bundle.putSerializable("EV", tEvent);
                                fragmentAddTask.setArguments(bundle);
                                ((MainActivity) context).callFragment(fragmentAddTask);
                            }
                            // listeners.onItemClicks(commonModelArrayList.get(position).geTTaskRecordId() + "");
                        }
                    });
                }
                else
                {
                    myViewHolder.tvEvent.setText(model.getTaskName());
                    myViewHolder.tvDate.setText("");
                    myViewHolder.imgMenu.setVisibility(View.GONE);
                }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public TaskAdapter(ArrayList<TTask> imageModelArrayList, Fragment fragment, Context context, TEvent tEvent)
    {
        this.fragment = fragment;
        this.commonModelArrayList = imageModelArrayList;
        this.context = context;
        this.from = from;
        this.tEvent = tEvent;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvEvent,tvDate;
        CheckBox checkBox;
        ImageView imgShare,imgMenu;

        public MyViewHolder(View view)
        {
            super(view);
            tvEvent = (TextView) view.findViewById(R.id.tvEvent);
            tvDate= (TextView) view.findViewById(R.id.tvDate);
         //   checkBox = (CheckBox) view.findViewById(R.id.chkCheck);
            imgMenu = (ImageView) view.findViewById(R.id.imgOption);
        }
    }

    @Override
    public int getItemCount()
    {
        if(commonModelArrayList != null && commonModelArrayList.size()>0)
            return commonModelArrayList.size();
        else
            return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void notifyData(ArrayList<TTask> imageModelArrayList)
    {
        this.commonModelArrayList = imageModelArrayList;
        notifyDataSetChanged();
    }

    public void showDeleteDialog(final TTask tTask)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to delete it ?");
        builder.setTitle("Checklist");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteTask(tTask);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    Handler handler = new Handler(Looper.getMainLooper())
    {
        @Override
        public void handleMessage(Message msg) {
                ((FragmentAddEvent)fragment).getTasksOfEvent();
        }
    };

    public void deleteTask(final TTask tTask)
    {
        Map<String, String> params = new HashMap<>();
        params.put("txtTaskCode", tTask.getTaskId());

        APICall.makeCall(params, Utility.URL_DELETE_TASK, context, Request.Method.POST, true, true,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object)
                    {
                        try {
                            String result = object.toString();
                            Log.d("Response", result);
                            Toast.makeText(context,"Task deleted successfully",Toast.LENGTH_SHORT).show();

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    new DBThread(context).deleteTask(tTask.getTaskId());
                                    Message message = new Message();
                                    handler.handleMessage(message);
                                }
                            }).start();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Utility.dismissDialog();
                    }

                    @Override
                    public void onError(Object object)
                    {
                    }
                });
    }

}