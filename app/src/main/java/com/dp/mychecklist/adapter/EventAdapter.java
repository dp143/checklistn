package com.dp.mychecklist.adapter;

/**
 * Created by pramodpatil305 on 09-02-2018.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.fragments.FragmentAddTask;
import com.dp.mychecklist.fragments.FragmentContact;
import com.dp.mychecklist.fragments.FragmentEventList;
import com.dp.mychecklist.fragments.FragmentRecurringList;
import com.dp.mychecklist.fragments.FragmentRecurringReminder;
import com.dp.mychecklist.fragments.FragmentReminder;
import com.dp.mychecklist.fragments.FragmentAddEvent;
import com.dp.mychecklist.fragments.FragmentReminderList;
import com.dp.mychecklist.reminders.Reminder;
import com.dp.mychecklist.roomDB.DBThread;
import com.dp.mychecklist.roomDB.entities.TEvent;
import com.dp.mychecklist.roomDB.entities.TTask;
import com.dp.mychecklist.util.APICall;
import com.dp.mychecklist.util.SessionManager;
import com.dp.mychecklist.util.Utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Pramod on 21/12/17.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder>
{
    ArrayList<TEvent> commonModelArrayList;
    MyViewHolder myViewHolder;
    Fragment fragment;
    static Context context;
    String from;
    RecyclerView recyclerView;
    Activity activity;
    private Random mRandom = new Random();

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_item_new , parent, false);

        //View view = inflater.inflate(R.layout.sec_cat_templ_item,null);
        myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        try {
            final TEvent model = commonModelArrayList.get(position);
            myViewHolder.tvEvent.setText(model.getEventName());
            myViewHolder.tvDate.setText(Utility.getDateInDDMMYYYYHHMMSS(model.getEventDate(), false));

            myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (model.getTaskTypeId().equals("1")) {
                        FragmentAddEvent fragmentAddProject = new FragmentAddEvent();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("EV", commonModelArrayList.get(position));
                        fragmentAddProject.setArguments(bundle);
                        ((MainActivity) context).callFragment(fragmentAddProject);
                    } else if (model.getTaskTypeId().equals("2")) {
                        FragmentReminder fragmentAddProject = new FragmentReminder();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("EV", commonModelArrayList.get(position));
                        fragmentAddProject.setArguments(bundle);
                        ((MainActivity) context).callFragment(fragmentAddProject);
                    } else if (model.getTaskTypeId().equals("3")) {
                        FragmentRecurringReminder fragmentAddProject = new FragmentRecurringReminder();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("EV", commonModelArrayList.get(position));
                        fragmentAddProject.setArguments(bundle);
                        ((MainActivity) context).callFragment(fragmentAddProject);
                    }
                }
            });

            myViewHolder.imgMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  PopupMenu popup = new PopupMenu(context, myViewHolder.imgMenu);
                    PopupMenu popup = new PopupMenu(context, ((MyViewHolder)recyclerView.findViewHolderForAdapterPosition(position)).imgMenu);
                    popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
//I love you so much

                    //registering popup with OnMenuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            if (item.getItemId() == R.id.menuEdit) {
                                if (model.getTaskTypeId().equals("1")) {
                                    FragmentAddEvent fragmentAddProject = new FragmentAddEvent();
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("EV", commonModelArrayList.get(position));
                                    fragmentAddProject.setArguments(bundle);
                                    ((MainActivity) context).callFragment(fragmentAddProject);
                                } else if (model.getTaskTypeId().equals("2")) {
                                    FragmentReminder fragmentAddProject = new FragmentReminder();
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("EV", commonModelArrayList.get(position));
                                    fragmentAddProject.setArguments(bundle);
                                    ((MainActivity) context).callFragment(fragmentAddProject);
                                } else if (model.getTaskTypeId().equals("3")) {
                                    FragmentRecurringReminder fragmentAddProject = new FragmentRecurringReminder();
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("EV", commonModelArrayList.get(position));
                                    fragmentAddProject.setArguments(bundle);
                                    ((MainActivity) context).callFragment(fragmentAddProject);
                                }
                            } else if (item.getItemId() == R.id.menuShare) {
                                Bundle bundle = new Bundle();
                                FragmentContact fragmentContact = new FragmentContact();
                                bundle.putString("EV", commonModelArrayList.get(position).getEventId());
                                fragmentContact.setArguments(bundle);
                                ((MainActivity) context).callFragment(fragmentContact);
                            } else if (item.getItemId() == R.id.menuDelete) {
                                showDeleteDialog(commonModelArrayList.get(position));
                            }

                            return true;
                        }
                    });

                    popup.show();//showing popup menu
                }
            });

            if(!commonModelArrayList.get(position).getUserId().equals(new SessionManager(context).getStringValue(SessionManager.USER_ID)))
                myViewHolder.llMain.setBackgroundColor(context.getResources().getColor(R.color.grey_light));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public EventAdapter(ArrayList<TEvent> imageModelArrayList, Fragment fragment, Context context,RecyclerView recyclerView)
    {
        this.fragment = fragment;
        this.commonModelArrayList = imageModelArrayList;
        this.context = context;
        this.from = from;
        this.recyclerView = recyclerView;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvEvent,tvDate;
        LinearLayout llMain;
        CheckBox checkBox;
        ImageView imgShare,imgMenu;

        public MyViewHolder(View view)
        {
            super(view);
            tvEvent = (TextView) view.findViewById(R.id.tvEvent);
            tvDate= (TextView) view.findViewById(R.id.tvDate);
           // checkBox = (CheckBox) view.findViewById(R.id.chkCheck);
            llMain = (LinearLayout) view.findViewById(R.id.llMain);
          //  imgShare = (ImageView) view.findViewById(R.id.imgShare);
            imgMenu = (ImageView) view.findViewById(R.id.imgOption);
        }
    }

    @Override
    public int getItemCount()
    {
        if(commonModelArrayList != null && commonModelArrayList.size()>0)
            return commonModelArrayList.size();
        else
            return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void notifyData(ArrayList<TEvent> imageModelArrayList)
    {
        this.commonModelArrayList = imageModelArrayList;
        notifyDataSetChanged();
    }

    public void deleteEvent(final TEvent tEvent)
    {
        Map<String, String> params = new HashMap<>();
         params.put("txtEventCode", tEvent.getEventId());

        APICall.makeCall(params, Utility.URL_DELETE_EVENT, context, Request.Method.POST, true, true,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object)
                    {
                        try {
                            String result = object.toString();
                            Log.d("Response", result);
                            Toast.makeText(context,"Event deleted successfully",Toast.LENGTH_SHORT).show();

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    new DBThread(context).deleteEvent(tEvent.getEventId());
                                    Message message = new Message();
                                    if(fragment instanceof FragmentEventList) {
                                        message.what = 1;
                                    }
                                    else if(fragment instanceof FragmentRecurringList) {
                                        message.what = 2;
                                    }
                                    else if(fragment instanceof FragmentReminderList) {
                                        message.what = 3;
                                    }
                                    handler.handleMessage(message);
                                }
                            }).start();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Utility.dismissDialog();
                    }

                    @Override
                    public void onError(Object object)
                    {
                    }
                });
    }

    Handler handler = new Handler(Looper.getMainLooper())
    {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == 1)
                    ((FragmentEventList)fragment).getMyEventList();
                else if(msg.what == 2)
                    ((FragmentRecurringList)fragment).getMyEventList();
                else if(msg.what == 3)
                    ((FragmentReminderList)fragment).getMyEventList();
        }
    };

    public void showDeleteDialog(final TEvent tEvent)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to delete it ?");
        builder.setTitle("Checklist");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteEvent(tEvent);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}