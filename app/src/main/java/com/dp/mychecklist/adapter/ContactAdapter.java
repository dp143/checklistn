package com.dp.mychecklist.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.fragments.FragmentContact;
import com.dp.mychecklist.util.APICall;
import com.dp.mychecklist.util.ContactModel;
import com.dp.mychecklist.util.Utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> implements Filterable
{
    MyViewHolder myViewHolder;
    Fragment fragment;
    static Context context;
    String from;
    RecyclerView recyclerView;
    ArrayList<ContactModel> arrContacts;
    private ArrayList<ContactModel> contactListFiltered;

    String userName;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_contact_new,null);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(5,5,5,5);
        view.setLayoutParams(lp);
        myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder( MyViewHolder holder,final int position)
    {
        final ContactModel contactModel = contactListFiltered.get(position);
        holder.tvUsername.setText(contactModel.getName()+" - "+contactModel.getContactNumber());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareEvent(contactModel.getContactNumber());
            }
        });
        //   myViewHolder.tvEmail.setText(model.getContactNumber());
    }

    public ContactAdapter(ArrayList<ContactModel> arrContacts, Fragment fragment, Context context)
    {
        this.fragment = fragment;
        this.arrContacts = arrContacts;
        this.context = context;
        this.from = from;
        this.recyclerView = recyclerView;
        this.contactListFiltered = arrContacts;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvUsername;
        LinearLayout llMain;
        ImageView imageView;

        public MyViewHolder(View view)
        {
            super(view);
            tvUsername = (TextView) view.findViewById(R.id.tvName);
            llMain = (LinearLayout) view.findViewById(R.id.llMain);
            imageView = (ImageView) view.findViewById(R.id.imgUser);
        }
    }

    @Override
    public int getItemCount()
    {
        return contactListFiltered.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void shareEvent(final String mobileNo)
    {
        Map<String, String> params = new HashMap<>();

        params.put("txtContactsList",mobileNo);
        params.put("txtEventCode",((FragmentContact) fragment).eventCode);

        APICall.makeCall(params, Utility.URL_SHARE_EVENT, context, Request.Method.POST, true, true,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object)
                    {
                        try {
                            String result = object.toString();
                            Log.d("Response", result);
                            JSONObject jsonObject = new JSONObject(result);

                            if(!jsonObject.getString("message").startsWith("Event"))
                                Utility.showShareAppDialog(context);
                            else
                                Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

                            ((MainActivity) context).onBackPressed();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //  Utility.dismissDialog();
                    }

                    @Override
                    public void onError(Object object)
                    {
                        if(object instanceof VolleyError)
                        {
                            VolleyError error= (VolleyError) object;

                           /* Log.d("Checklist", "Error: " + error
                                    + "\nStatus Code " + error.networkResponse.statusCode
                                    + "\nResponse Data " + error.networkResponse.data
                                    + "\nCause " + error.getCause()
                                    + "\nmessage" + error.getMessage());*/
                        }
                        Log.d("Error",object.toString());
                    }
                });
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = arrContacts;
                } else {
                    ArrayList<ContactModel> filteredList = new ArrayList<>();
                    for (ContactModel row : arrContacts) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getContactNumber().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<ContactModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }



}
