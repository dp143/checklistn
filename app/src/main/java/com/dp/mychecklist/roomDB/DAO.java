package com.dp.mychecklist.roomDB;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.dp.mychecklist.roomDB.entities.TEvent;
import com.dp.mychecklist.roomDB.entities.TTask;

import java.util.List;

/**
 * Created by pramodpatil305 on 22-05-2018.
 */
@Dao
public interface DAO
{
    @Insert
    public long insertEvent(TEvent tEvent);

    @Insert
    public void insertEvents(TEvent... tEvent);

    @Insert
    public long insertTask(TTask tTask);

    @Insert
    public void insertTasks(TTask... tTask);

    @Query("delete from TTask")
    public void deleteTasks();

    @Query("delete from TEvent")
    public void deleteEvents();

    @Update
    public int updateEvent(TEvent tEvent);

    @Update
    public int updateTask(TTask tTask);

    @Query("Update TEvent set deleted = 1 where eventId=:eventId")
    public void deleteEvent(String eventId);

    @Query("Update TTask set deleted = 1 where taskId=:taskId")
    public void deleteTask(String taskId);

    @Query("select * from TEvent where eventId =:eventId")
    public TEvent getEventById(String eventId);

    @Query("select * from TTask where taskId =:taskId")
    public TTask getTaskById(String taskId);

    @Query("select * from TEVENT where taskTypeId = :taskTypeId AND (deleted is NULL OR deleted=0)")
  //  @Query("select * from TEVENT where taskTypeId = :taskTypeId AND deleted=1")
    public List<TEvent> getEvents(String taskTypeId);

    @Query("select * from TTask where fk_eventId=:eventId AND (deleted is NULL OR deleted=0)")
    public List<TTask> getTasksOfEvent(String eventId);
}
