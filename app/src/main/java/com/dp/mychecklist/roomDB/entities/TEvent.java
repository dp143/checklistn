package com.dp.mychecklist.roomDB.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class TEvent implements Serializable
{
    @PrimaryKey(autoGenerate = true)
    private int eventRecordId;

    private String eventId,eventName, eventDate, eventReminderDate, eventDescription, eventSt, eventDT, taskTypeId,
    eventRepeateDays,userId, deleted;

    public String getUserId() {
        return userId;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEventRepeateDays() {
        return eventRepeateDays;
    }

    public void setEventRepeateDays(String eventRepeateDays) {
        this.eventRepeateDays = eventRepeateDays;
    }

    public int getEventRecordId() {
        return eventRecordId;
    }

    public void setEventRecordId(int eventRecordId) {
        this.eventRecordId = eventRecordId;
    }

    public String getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(String taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    public String getEventId() {
        return eventId;
    }

    public String getEventReminderDate() {
        return eventReminderDate;
    }

    public void setEventReminderDate(String eventReminderDate) {
        this.eventReminderDate = eventReminderDate;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventSt() {
        return eventSt;
    }

    public void setEventSt(String eventSt) {
        this.eventSt = eventSt;
    }

    public String getEventDT() {
        return eventDT;
    }

    public void setEventDT(String eventDT) {
        this.eventDT = eventDT;
    }
}
