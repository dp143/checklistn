package com.dp.mychecklist.roomDB.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class TTask implements Serializable
{
    @PrimaryKey(autoGenerate = true)
    private int taskRecordId;

    private String taskId, taskName, taskDate, taskDT, taskReminderDate, taskSt, taskDescription, fk_eventId,deleted;

    public int getTaskRecordId() {
        return taskRecordId;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public void setTaskRecordId(int taskRecordId) {
        this.taskRecordId = taskRecordId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(String taskDate) {
        this.taskDate = taskDate;
    }

    public String getTaskDT() {
        return taskDT;
    }

    public void setTaskDT(String taskDT) {
        this.taskDT = taskDT;
    }

    public String getTaskReminderDate() {
        return taskReminderDate;
    }

    public void setTaskReminderDate(String taskReminderDate) {
        this.taskReminderDate = taskReminderDate;
    }

    public String getTaskSt() {
        return taskSt;
    }

    public void setTaskSt(String taskSt) {
        this.taskSt = taskSt;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getFk_eventId() {
        return fk_eventId;
    }

    public void setFk_eventId(String fk_eventId) {
        this.fk_eventId = fk_eventId;
    }
}
