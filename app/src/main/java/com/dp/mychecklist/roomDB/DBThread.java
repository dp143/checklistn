package com.dp.mychecklist.roomDB;

import android.content.Context;

import com.dp.mychecklist.roomDB.entities.TEvent;
import com.dp.mychecklist.roomDB.entities.TTask;

import java.util.ArrayList;

/**
 * Created by pramodpatil305 on 23-06-2018.
 */

public class DBThread {
    Context context;
    static ChecklistDB checklistDB;

    public DBThread(final Context context) {
        this.context = context;
        if (checklistDB == null)
            checklistDB = ChecklistDB.getInstance(context);
    }

    public long insertEvent(TEvent event)
    {
        return checklistDB.daoAccess().insertEvent(event);
    }

    public void insertEvents(final TEvent[] event)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<event.length;i++) {
                    if(checklistDB.daoAccess().getEventById(event[i].getEventId()) == null)
                        checklistDB.daoAccess().insertEvent(event[i]);
                }
            }
        }).start();
    }

    public long insertTask(TTask tTask)
    {
        return checklistDB.daoAccess().insertTask(tTask);
    }

    public void insertTasks(final TTask[] tTask)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<tTask.length;i++) {
                    if(checklistDB.daoAccess().getTaskById(tTask[i].getTaskId()) == null)
                        checklistDB.daoAccess().insertTask(tTask[i]);
                }
            }
        }).start();
    }

    public TEvent getEventById(String id)
    {
        return checklistDB.daoAccess().getEventById(id);
    }

    public int updateEvent(TEvent event)
    {
        return checklistDB.daoAccess().updateEvent(event);
    }

    public int updateTask(TTask tTask)
    {
        return checklistDB.daoAccess().updateTask(tTask);
    }

    public void deleteEvent(final String eventId)
    {
        checklistDB.daoAccess().deleteEvent(eventId);
    }

    public void deleteTask(final String taskId)
    {
        checklistDB.daoAccess().deleteTask(taskId);
    }
    public ArrayList<TEvent> getEvents(String type)
    {
        return (ArrayList<TEvent>) checklistDB.daoAccess().getEvents(type);
    }
    public ArrayList<TTask> getTasks(String eventId)
    {
        return (ArrayList<TTask>) checklistDB.daoAccess().getTasksOfEvent(eventId);
    }

    public void deleteDB()
    {
        checklistDB.daoAccess().deleteEvents();
        checklistDB.daoAccess().deleteTasks();
    }
}
