package com.dp.mychecklist.roomDB;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.dp.mychecklist.roomDB.entities.TEvent;
import com.dp.mychecklist.roomDB.entities.TTask;

import java.util.concurrent.Executors;

/**
 * Created by pramodpatil305 on 22-05-2018.
 */

@Database(entities = {TEvent.class, TTask.class},
        version = 1, exportSchema = false)

public abstract class ChecklistDB extends RoomDatabase
{
    public abstract DAO daoAccess();

    private static ChecklistDB INSTANCE;

    public synchronized static ChecklistDB getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = buildDatabase(context);
        }
        return INSTANCE;
    }

    private static ChecklistDB buildDatabase(final Context context) {
        return Room.databaseBuilder(context,
                ChecklistDB.class,
                "checklist-db")
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                            @Override
                            public void run() {
                                // new DBThread(context).insertInitialData();
                            }
                        });
                    }
                })
                .fallbackToDestructiveMigration()
                .build();
    }

}
