package com.dp.mychecklist.reminders;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Hashtable;
import java.util.regex.Pattern;

/**
 * Created by Sagar on 03-10-2016.
 */
public class EventManagerUtil {
    public static final String EVENT_SP_PREFIX = "event_noti_";

    public static Hashtable<String, PendingNotificationModel> getAllPendingNotifications(Context context) {
        Hashtable<String, PendingNotificationModel> list = new Hashtable<>();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        int i = 0;
        while (true) {
            if (sp.getString(EVENT_SP_PREFIX + i, "").equals(""))
                break;
            else {
                String str[] = sp.getString(EVENT_SP_PREFIX + i, "").split(Pattern.quote("~"));
                PendingNotificationModel model = new PendingNotificationModel(str.length > 0 ? str[0] : "", str.length > 1 ? str[1] : "", str.length > 2 ? str[2] : "");
                list.put(str[0], model);
            }
            i++;
        }

        return list;
    }

    public static String getNextEventNotificationKey(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        String rem = "";
        int i = 0;
        while (true) {
            if (sp.getString(EVENT_SP_PREFIX + i, "").equals(""))
                break;
            else
                rem = EVENT_SP_PREFIX + i;
            i++;
        }

        int n = 0;
        if (!rem.equals(""))
            n = Integer.parseInt(rem.substring(EVENT_SP_PREFIX.length()));
        else
            n = -1;

        return EVENT_SP_PREFIX + (n + 1);
    }

    public static void deleteAllNotifications(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        int i = 0;
        while (true) {
            if (sp.getString(EVENT_SP_PREFIX + i, "").equals(""))
                break;
            else {
                editor.remove(EVENT_SP_PREFIX + i);
                editor.commit();
            }
            i++;
        }
    }
}
