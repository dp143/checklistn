package com.dp.mychecklist.reminders;

/**
 * Created by Sagar on 03-10-2016.
 */
public class PendingNotificationModel {
    private String id = "";
    private String reminderName = "";
    private String label = "";

    public PendingNotificationModel(String id, String reminderName,String message) {
        setId(id);
        setReminderName(reminderName);
        setLabel(message);
    }

    public String getReminderName() {
        return reminderName;
    }

    public void setReminderName(String reminderName) {
        this.reminderName = reminderName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
