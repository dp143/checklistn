/* Copyright 2014 Sheldon Neilson www.neilson.co.za
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package com.dp.mychecklist.reminders;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;

import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.roomDB.entities.TEvent;
import com.dp.mychecklist.roomDB.entities.TTask;


/**
 * Created by Sagar on 03-10-2016.
 */
public class AlarmAlertBroadcastReceiver extends BroadcastReceiver {

    final String REMINDER_GROUP_KEY = "Reminders";
    Context mContext;
    Object object;
    TEvent tEvent;
    TTask tTask;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        StaticWakeLock.lockOn(context);
      //  Toast.makeText(context,"Alarm Received",Toast.LENGTH_SHORT).show();
        Bundle bundle = intent.getBundleExtra("DATA");
        Reminder reminder = null;
        try {
            reminder = (Reminder) bundle.getSerializable("ALARM");
            object =  bundle.getSerializable("OBJ");
            String message = "";
            if(object instanceof TEvent) {
                tEvent = (TEvent) object;
                message = "The reminder for the - "+tEvent.getEventName();
            }
            else if(object instanceof TTask) {
                tTask = (TTask) object;
                message = "The reminder for the - "+tTask.getTaskName();
            }

            if (reminder != null)
                showReminder(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
      //  Log.d("Event : onReceive", reminder.getReminderName() + ": " + reminder.getStartTime().getTimeInMillis());
        StaticWakeLock.lockOff(context);
    }

    public void showReminder(String message)
    {
        String CHANNEL_ID = "my_channel_01";
        CharSequence name = "Checklist";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;

        Intent intent = new Intent(mContext ,MainActivity.class);

        // use System.currentTimeMillis() to have a unique ID for the pending intent
        PendingIntent accIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // build notification
        // the addAction re-use the same intent to keep the eyemagnett short
        NotificationCompat.Builder n  = new NotificationCompat.Builder(mContext)
                .setContentTitle("CheckLog")
                .setContentText(message)
                .setSmallIcon(R.drawable.ic_calendar)
                .setContentIntent(accIntent)
                .setAutoCancel(true)
                .setChannelId(CHANNEL_ID)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager notificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
            mChannel.setShowBadge(true);
        }

        notificationManager.notify(0, n.build());
    }


    private Uri getSoundUri(Reminder reminder) {
        Uri alarmSound = Uri.parse(reminder.getAlarmTonePath());
        if (alarmSound == null) {
            alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            if (alarmSound == null) {
                alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            }
        }

        return alarmSound;
    }

    private long[] getVibrationPattern() {
        return new long[]{500};
    }

    /**
     * Returns a CharSequence that concatenates the specified array of CharSequence
     * objects and then applies a list of zero or more tags to the entire range.
     *
     * @param content an array of character sequences to apply a style to
     * @param tags    the styled span objects to apply to the content
     *                such as android.text.style.StyleSpan
     */
    private static CharSequence apply(CharSequence[] content, Object... tags) {
        SpannableStringBuilder text = new SpannableStringBuilder();
        openTags(text, tags);
        for (CharSequence item : content) {
            text.append(item);
        }
        closeTags(text, tags);
        return text;
    }

    /**
     * Iterates over an array of tags and applies them to the beginning of the specified
     * Spannable object so that future text appended to the text will have the styling
     * applied to it. Do not call this method directly.
     */
    private static void openTags(Spannable text, Object[] tags) {
        for (Object tag : tags) {
            text.setSpan(tag, 0, 0, Spannable.SPAN_MARK_MARK);
        }
    }

    /**
     * "Closes" the specified tags on a Spannable by updating the spans to be
     * endpoint-exclusive so that future text appended to the end will not take
     * on the same styling. Do not call this method directly.
     */
    private static void closeTags(Spannable text, Object[] tags) {
        int len = text.length();
        for (Object tag : tags) {
            if (len > 0) {
                text.setSpan(tag, 0, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else {
                text.removeSpan(tag);
            }
        }
    }

    /**
     * Returns a CharSequence that applies boldface to the concatenation
     * of the specified CharSequence objects.
     */
    public static CharSequence bold(CharSequence... content) {
        return apply(content, new StyleSpan(Typeface.BOLD));
    }

    /**
     * Returns a CharSequence that applies italics to the concatenation
     * of the specified CharSequence objects.
     */
    public static CharSequence italic(CharSequence... content) {
        return apply(content, new StyleSpan(Typeface.ITALIC));
    }

    /**
     * Returns a CharSequence that applies a foreground color to the
     * concatenation of the specified CharSequence objects.
     */
    public static CharSequence color(int color, CharSequence... content) {
        return apply(content, new ForegroundColorSpan(color));
    }
}
