package com.dp.mychecklist.reminders;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.util.Log;

import com.dp.mychecklist.roomDB.entities.TEvent;
import com.dp.mychecklist.roomDB.entities.TTask;

import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Pramod on 03-10-2016.
 */
public class Reminder implements Serializable {

    private static final long serialVersionUID = 8699489847426803789L;
    private int id;
    private Boolean alarmActive = true;
    private String alarmTonePath = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString();
    private Boolean vibrate = true;
    private String reminderName = "";
    private Calendar startTime = null;
    private Calendar endTime = null;
    private int startUID = 0;
    private int endUID = 0;
    private String serverId = "";
    private String status = "new";
    private String label = "";
    private String eventId = "", taskId="", subTaskId = "", projectId="0";
    int type= 0;
    private TEvent tEvent;
    private TTask tTask;

    public TEvent gettEvent() {
        return tEvent;
    }

    public void settEvent(TEvent tEvent) {
        this.tEvent = tEvent;
    }

    public TTask gettTask() {
        return tTask;
    }

    public void settTask(TTask tTask) {
        this.tTask = tTask;
    }

    public Reminder() {
    }

    /**
     * @return the alarmActive
     */
    public Boolean getAlarmActive() {
        return alarmActive;
    }

    /**
     * @param alarmActive the alarmActive to set
     */
    public void setAlarmActive(Boolean alarmActive) {
        this.alarmActive = alarmActive;
    }

    public Calendar getStartTime() {
        return startTime;
    }

    public String getReminderTimeString() {
        String time = "";

        if (startTime != null) {
            if (startTime.get(Calendar.HOUR_OF_DAY) <= 9)
                time += "0";
            time += String.valueOf(startTime.get(Calendar.HOUR_OF_DAY));
            time += ":";

            if (startTime.get(Calendar.MINUTE) <= 9)
                time += "0";
            time += String.valueOf(startTime.get(Calendar.MINUTE));
        }
        return time;
    }

    public String getEndTimeString() {

        String time = "";
        if (endTime != null) {
            if (endTime.get(Calendar.HOUR_OF_DAY) <= 9)
                time += "0";
            time += String.valueOf(endTime.get(Calendar.HOUR_OF_DAY));
            time += ":";

            if (endTime.get(Calendar.MINUTE) <= 9)
                time += "0";
            time += String.valueOf(endTime.get(Calendar.MINUTE));
        }
        return time;
    }

    public void setStartTime(Calendar time) {

        startTime = time;
    }

    public String getAlarmTonePath() {
        return alarmTonePath;
    }

    public void setAlarmTonePath(String alarmTonePath) {
        this.alarmTonePath = alarmTonePath;
    }

    public Boolean getVibrate() {
        return vibrate;
    }


    public void setVibrate(Boolean vibrate) {
        this.vibrate = vibrate;
    }

    public String getReminderName() {
        return reminderName;
    }

    public void setReminderName(String alarmName) {
        this.reminderName = alarmName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Calendar getEndTime() {
        return endTime;
    }

    public void setEndTime(Calendar endTime) {
        this.endTime = endTime;
    }

    public void schedule(Context context) {
        try {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            Intent myIntent = new Intent(context, AlarmAlertBroadcastReceiver.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("ALARM", this);

            if(gettEvent() != null)
                bundle.putSerializable("OBJ",gettEvent());
            else if(gettTask() !=null)
                bundle.putSerializable("OBJ",gettTask());

            myIntent.putExtra("DATA", bundle);
            Log.d("Millisg", getStartTime().getTimeInMillis() + "");
            if (getStartTime().getTimeInMillis() > System.currentTimeMillis()) {
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, getId(), myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                //alarmManager.set(AlarmManager.RTC_WAKEUP, getStartTime().getTimeInMillis(), pendingIntent);
                alarmManager.set(AlarmManager.RTC_WAKEUP, getStartTime().getTimeInMillis(), pendingIntent);
                Log.d("Reminder Set", getId() + "");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void schedule(Context context, long repeatTime) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent myIntent = new Intent(context, AlarmAlertBroadcastReceiver.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("ALARM",this);
        myIntent.putExtra("DATA", bundle);

        Log.d("Millisg",getStartTime().getTimeInMillis()+"");
        if (getStartTime().getTimeInMillis() > System.currentTimeMillis())
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, getId(), myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            //alarmManager.set(AlarmManager.RTC_WAKEUP, getStartTime().getTimeInMillis(), pendingIntent);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,getStartTime().getTimeInMillis(), repeatTime, pendingIntent);
            Log.d("Reminder Set",getId()+"");
        }
    }

    private Calendar getAlarmTime(Date date, Calendar calendar) {
        Calendar time = Calendar.getInstance();
        if (date != null)
            time.setTimeInMillis(date.getTime());
        time.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));
        time.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
        time.set(Calendar.SECOND, 0);
        time.set(Calendar.MILLISECOND, 0);
        return time;
    }

    public void cancel(Context context) {
        Intent myIntent = new Intent(context, AlarmAlertBroadcastReceiver.class);
        myIntent.putExtra("alarm", this);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager.cancel(pendingIntent);
    }

    public static Calendar getStartTime(String time) {
        Calendar calendar = Calendar.getInstance();
        if (!time.equals("") && time.contains(":")) {
            String[] times = time.split(Pattern.quote(":"));
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(times[0].trim()));
            calendar.set(Calendar.MINUTE, Integer.parseInt(times[1].trim()));
        }
        return calendar;
    }

    public static Calendar getTime(String time) {

        Calendar calendar = null;
        if (!time.equals("") && time.contains(":")) {
            calendar = Calendar.getInstance();
            String[] times = time.split(Pattern.quote(":"));
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(times[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(times[1]));
        }
        return calendar;
    }

    public static Date getDate(String strDate) {

        SimpleDateFormat mDateFormat = new SimpleDateFormat(DateTimeUtils.FORMAT_TIME_STAMP_MILLIS);

        try {
            return mDateFormat.parse(strDate);
        } catch (Exception e) {
//            e.printStackTrace();
        }

        return null;
    }

    public static String getDateDDMMMYYY(Date date) {
        return DateTimeUtils.millisToDateFormat(date.getTime(), DateTimeUtils.FORMAT_DATE_DDMMMYYYY_SIMPLE);
    }

    public static int binaryToDecimal(String binaryValue) {
        int decimal = Integer.parseInt(binaryValue, 2);
        return decimal;
    }

    public static String toBinary(int number) {
        String bin = "";
        int copyOfInput = number;
        while (copyOfInput > 0) {
            bin += (byte) (copyOfInput % 2);
            copyOfInput = copyOfInput / 2;
        }
        return bin;
    }

    public static String reverseString(String input) {
        StringBuilder builder = new StringBuilder();
        char[] try1 = input.toCharArray();
        for (int i = try1.length - 1; i >= 0; i--)
            builder.append(try1[i]);

        return builder.toString();
    }

    public static String getDisplayDate(Date date) {
        if (date != null)
            return getDayName(date) + ", " + getDateDDMMMYYY(date);
        else
            return "";
    }

    static String getDayName(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, date.getDay());
        calendar.set(Calendar.MONTH, date.getMonth());
        calendar.set(Calendar.YEAR, date.getYear());
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
        return symbols.getWeekdays()[calendar.get(Calendar.DAY_OF_WEEK)];
    }

    public int getStartUID() {
        return startUID;
    }

    public void setStartUID(int startUID) {
        this.startUID = startUID;
    }

    public int getEndUID() {
        return endUID;
    }

    public void setEndUID(int endUID) {
        this.endUID = endUID;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static String getDateToString(Date date) {
        return date != null ? DateTimeUtils.millisToDateFormat(date.getTime(), DateTimeUtils.FORMAT_TIME_STAMP_MILLIS) : "";
    }

    public void setStartTime(int hour, int minute) {
        startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR, hour);
        startTime.set(Calendar.MINUTE, minute);
        startTime.set(Calendar.SECOND, 0);
    }

    public void setStartTime(int dateYear, int dateMonth, int dateDay, int timeHours, int timeMinute) {

        startTime = Calendar.getInstance();
        startTime.set(Calendar.DAY_OF_MONTH, dateDay);
        startTime.set(Calendar.MONTH, dateMonth);
        startTime.set(Calendar.YEAR, dateYear);
        startTime.set(Calendar.HOUR_OF_DAY, timeHours);
        startTime.set(Calendar.MINUTE, timeMinute);
        startTime.set(Calendar.SECOND, 0);
    }

    public void setTime(String strReminder)
    {

    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getSubTaskId() {
        return subTaskId;
    }

    public void setSubTaskId(String subTaskId) {
        this.subTaskId = subTaskId;
    }

    public int getType() {
        return type;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void setType(int type) {
        this.type = type;
    }
}
