package com.dp.mychecklist.reminders;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Josy-GOQii on 10-03-2016.
 */
public class DateTimeUtils {

    public final static String FORMAT_TIME_STAMP_MILLIS = "yyyy-MM-dd HH:mm:ss.SSS";
    public final static String FORMAT_TIME_STAMP_SECS = "yyyy-MM-dd HH:mm:ss";
    public final static String FORMAT_TIME_STAMP_MINS = "yyyy-MM-dd HH:mm";
    public final static String FORMAT_DATE_YYYYMMDD = "yyyy-MM-dd";
    public final static String FORMAT_DATE_DDMMYYYY = "dd-MM-yyyy";
    public final static String FORMAT_DATE_DDMMYYYY_SIMPLE = "dd MM yyyy";
    public final static String FORMAT_DATE_YYYYMMMDD_SIMPLE = "yyyy MMM dd";
    public final static String FORMAT_DATE_DDMMMYYYY_SIMPLE = "dd MMM yyyy";
    public final static String FORMAT_24_HOURS = "HH:mm";
    public final static String FORMAT_12_HOURS = "hh:mm a";
    private static SimpleDateFormat mDateFormat;

    /**
     * returns current time in millis
     */
    public static long getCurrentTimeMillis() {
        return Calendar.getInstance().getTimeInMillis();
    }

    /**
     * returns minutes of milliseconds
     */
    public static long millisToMinutes(long millis) {
        return TimeUnit.MILLISECONDS.toMinutes(millis);
    }

    /**
     * returns seconds of milliseconds
     */
    public static long millisToSeconds(long millis) {
        return TimeUnit.MILLISECONDS.toSeconds(millis);
    }

    /**
     * returns Timestamp of the milliseconds
     */
    public static String millisToTimeStamp(long millis) {
        mDateFormat = new SimpleDateFormat(FORMAT_TIME_STAMP_MILLIS);
        return mDateFormat.format(millis).trim();
    }

//    /**
//     *  returns 24hrs time of the milliseconds
//     * */
//    public static String millisToTime(long millis) {
//        mDateFormat = new SimpleDateFormat(FORMAT_24_HOURS);
//        return mDateFormat.format(millis).trim();
//    }
//
//    /**
//     *  returns time in AM/PM of the milliseconds
//     * */
//    public static String millisToSimpleTime(long millis) {
//        mDateFormat = new SimpleDateFormat(FORMAT_12_HOURS);
//        return mDateFormat.format(millis).trim();
//    }

    /**
     * returns date in milliseconds from the SimpleDateFormat passed
     */
    public static long dateFormatToMillis(String timestamp, String format) {
        try {
            return getDateFormat(format).parse(timestamp).getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * returns date in SimpleFormat from the milliseconds passed
     */
    public static String millisToDateFormat(long millis, String format) {
        try {
            return getDateFormat(format).format(millis).trim();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * returns milliseconds of a specific date
     */
    public static long specificDateToMillis(int month, int day, int year, int hour, int minute, int seconds) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, day, hour, minute, seconds);

        return c.getTimeInMillis();
    }

    public static SimpleDateFormat getDateFormat(String format) {
        mDateFormat = new SimpleDateFormat(format);
        return mDateFormat;
    }


    public static String getTimeToString(Calendar calendar, boolean is24Hours) {
        if (calendar != null) {
            String aTime;
            int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);

            if (!is24Hours) {
                int hours = hourOfDay;
                int minutes = minute;
                String hr = "";
                String timeSet = "";
                if (hours > 12) {
                    hours -= 12;
                    timeSet = "PM";
                } else if (hours == 0) {
                    hours += 12;
                    timeSet = "AM";
                } else if (hours == 12)
                    timeSet = "PM";
                else
                    timeSet = "AM";

                if (hours <= 9)
                    hr = "0" + hours;
                else
                    hr = String.valueOf(hours);

                String min = "";
                if (minutes < 10)
                    min = "0" + minutes;
                else
                    min = String.valueOf(minutes);

                aTime = new StringBuilder().append(hr).append(':')
                        .append(min).append(" ").append(timeSet).toString();
            } else {
                String hr = "";
                if (hourOfDay <= 9)
                    hr = "0" + hourOfDay;
                else
                    hr = String.valueOf(hourOfDay);

                String min = "";
                if (minute < 10)
                    min = "0" + minute;
                else
                    min = String.valueOf(minute);

                aTime = new StringBuilder().append(hr).append(':')
                        .append(min).toString();
            }

            return aTime;
        } else {
            return "";
        }
    }

    public static String getTimeToString(String timeHHMM, boolean is24Hours) {
        if (timeHHMM != null && timeHHMM.contains(":")) {
            String aTime;
            String[] hm = timeHHMM.split(":");
            int hourOfDay = Integer.parseInt(hm[0]);
            int minute = Integer.parseInt(hm[1]);

            if (!is24Hours) {
                int hours = hourOfDay;
                int minutes = minute;
                String hr = "";
                String timeSet = "";
                if (hours > 12) {
                    hours -= 12;
                    timeSet = "PM";
                } else if (hours == 0) {
                    hours += 12;
                    timeSet = "AM";
                } else if (hours == 12)
                    timeSet = "PM";
                else
                    timeSet = "AM";

                if (hours <= 9)
                    hr = "0" + hours;
                else
                    hr = String.valueOf(hours);

                String min = "";
                if (minutes < 10)
                    min = "0" + minutes;
                else
                    min = String.valueOf(minutes);

                aTime = new StringBuilder().append(hr).append(':')
                        .append(min).append(" ").append(timeSet).toString();
            } else {
                String hr = "";
                if (hourOfDay <= 9)
                    hr = "0" + hourOfDay;
                else
                    hr = String.valueOf(hourOfDay);

                String min = "";
                if (minute < 10)
                    min = "0" + minute;
                else
                    min = String.valueOf(minute);

                aTime = new StringBuilder().append(hr).append(':')
                        .append(min).toString();
            }

            return aTime;
        } else {
            return "";
        }
    }

    public static String getTimeToString(int hourOfDay, int minute, boolean is24Hours) {
        String aTime;
        if (!is24Hours) {
            int hours = hourOfDay;
            int minutes = minute;
            String timeSet = "";
            if (hours > 12) {
                hours -= 12;
                timeSet = "PM";
            } else if (hours == 0) {
                hours += 12;
                timeSet = "AM";
            } else if (hours == 12)
                timeSet = "PM";
            else
                timeSet = "AM";

            String hr = "";
            if (hours <= 9)
                hr = "0" + hours;
            else
                hr = String.valueOf(hours);

            String min = "";
            if (minutes < 10)
                min = "0" + minutes;
            else
                min = String.valueOf(minutes);

            aTime = new StringBuilder().append(hr).append(':')
                    .append(min).append(" ").append(timeSet).toString();
        } else {

            String hr = "";
            if (hourOfDay <= 9)
                hr = "0" + hourOfDay;
            else
                hr = String.valueOf(hourOfDay);

            String min = "";
            if (minute < 10)
                min = "0" + minute;
            else
                min = String.valueOf(minute);
            aTime = new StringBuilder().append(hr).append(':')
                    .append(min).toString();
        }
        return aTime;
    }

    public static Calendar getStringToCalendar(String yyyyMMdd_HHMMS) {
        Calendar calendar = Calendar.getInstance();
        try {
            String date = yyyyMMdd_HHMMS.substring(0, yyyyMMdd_HHMMS.indexOf(" "));
            String[] arrDate = date.split("-");
            calendar.set(Calendar.YEAR, parseInt(arrDate[2]));
            calendar.set(Calendar.MONTH, parseInt(arrDate[1]));
            calendar.set(Calendar.DAY_OF_MONTH, parseInt(arrDate[0]));
        } catch (Exception e) {
            new Exception("Invalid date string " + yyyyMMdd_HHMMS + "\n" + e.toString()).printStackTrace();
        }

        try {
            String time = yyyyMMdd_HHMMS.substring(yyyyMMdd_HHMMS.indexOf(" ") + 1, yyyyMMdd_HHMMS.length());
            String[] arrTime = time.split(":");
            calendar.set(Calendar.HOUR_OF_DAY, parseInt(arrTime[0]));
            calendar.set(Calendar.MINUTE, parseInt(arrTime[0]));
            calendar.set(Calendar.SECOND, 0);
        } catch (Exception e) {
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
        }
        return calendar;
    }

    public static String getDateTimeInFormatyyyyMMdd_hhmm(int year, int month, int day, int hh, int mm) {
        return  getInTwoDigits(day)+ "-" + getInTwoDigits(month) + "-" + year + " " + getInTwoDigits(hh) + ":" + getInTwoDigits(mm);
    }

    public static String getInTwoDigits(int n) {
        String no = n + "";
        if (n < 10) {
            no = "0" + n;
        }
        return no;
    }

    private static int parseInt(String n) {
        return Integer.parseInt(n);
    }

    public static String getCurrentDate(String format) {
        SimpleDateFormat sdfDate = new SimpleDateFormat(format);//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }
}
