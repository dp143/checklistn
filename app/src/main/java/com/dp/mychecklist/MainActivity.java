package com.dp.mychecklist;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.dp.mychecklist.fragments.FragmentChangePassword;
import com.dp.mychecklist.fragments.FragmentContact;
import com.dp.mychecklist.fragments.FragmentProfile;
import com.dp.mychecklist.fragments.FragmentRecurringList;
import com.dp.mychecklist.fragments.FragmentRecurringReminder;
import com.dp.mychecklist.fragments.FragmentReminder;
import com.dp.mychecklist.fragments.FragmentAddEvent;
import com.dp.mychecklist.fragments.FragmentAddTask;
import com.dp.mychecklist.fragments.FragmentEventList;
import com.dp.mychecklist.fragments.FragmentLogin;
import com.dp.mychecklist.fragments.FragmentRegister;
import com.dp.mychecklist.fragments.FragmentReminderList;
import com.dp.mychecklist.util.APICall;
import com.dp.mychecklist.util.GetSharedEvent;
import com.dp.mychecklist.util.PermissionResultCallback;
import com.dp.mychecklist.util.PermissionUtils;
import com.dp.mychecklist.util.SessionManager;
import com.dp.mychecklist.util.Utility;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, PermissionResultCallback
{
    @BindView(R.id.llProfile)
    LinearLayout llProfile;
    @BindView(R.id.llEvents)
    LinearLayout llEvents;
    @BindView(R.id.llReminders)
    LinearLayout llReminder;
    @BindView(R.id.llRecurring)
    LinearLayout llRecurring;
    @BindView(R.id.llFriends)
    LinearLayout llFriends;
    @BindView(R.id.llBottomMenuResident)
    public LinearLayout llBottomMenuResident;

    boolean doubleBackToExitPressedOnce= false;
    public Menu menu;
    public Fragment currentFragment;
    View view;
    Toolbar toolbar;
    PermissionUtils permissionUtils;
    ArrayList<String> arrPermissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setNavigationIcon(R.drawable.app_logo_inside);

        ButterKnife.bind(this);
        if(new SessionManager(this).getStringValue(SessionManager.USER_ID).equals(""))
        {
            callFragment(new FragmentLogin());
           // callFragment(new FragmentAddEvent());
            llBottomMenuResident.setVisibility(View.GONE);
        }
        else
        {
          //  callFragment(new FragmentAddEvent());
            FragmentReminderList fragmentEventList=new FragmentReminderList();
            callFragment(fragmentEventList);
            llBottomMenuResident.setVisibility(View.VISIBLE);
            setTitle("Reminders");
            startService(new Intent(MainActivity.this, GetSharedEvent.class));
        }

        llFriends.setOnClickListener(this);
        llEvents.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        llRecurring.setOnClickListener(this);
        llReminder.setOnClickListener(this);

        view = findViewById(android.R.id.content);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        permissionUtils = new PermissionUtils(MainActivity.this);
        arrPermissions = new ArrayList<>();
        arrPermissions.add(Manifest.permission.READ_CONTACTS);
        arrPermissions.add(Manifest.permission.WRITE_CONTACTS);
        permissionUtils.check_permission(arrPermissions,1);
        }

    public void setTitle(String title)
    {
        /*TextView tvTitle= (TextView) findViewById(R.id.title);
        tvTitle.setText(title);*/
        getSupportActionBar().setTitle(title);
    }

    public void callFragment(android.support.v4.app.Fragment fragment) {
        try {
            if(fragment instanceof FragmentReminder || fragment instanceof FragmentRecurringReminder ||
                    fragment instanceof FragmentAddEvent || fragment instanceof FragmentAddTask ||
                    fragment instanceof FragmentRegister ||   fragment instanceof FragmentChangePassword) {
                toolbar.setNavigationIcon(R.drawable.ic_back);
                llBottomMenuResident.setVisibility(View.GONE);
            }
            else if(fragment instanceof FragmentLogin)
                llBottomMenuResident.setVisibility(View.GONE);
            else {
                toolbar.setNavigationIcon(R.drawable.app_logo_inside);
                llBottomMenuResident.setVisibility(View.VISIBLE);
            }

            android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out, R.anim.left_in, R.anim.right_out);
            transaction.replace(R.id.frame, fragment, fragment.getClass().toString());
            transaction.addToBackStack(null);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSelected(LinearLayout linearLayout)
    {
        ArrayList<LinearLayout> linearLayoutArrayList = new ArrayList<>();
        linearLayoutArrayList.add(llFriends);
        linearLayoutArrayList.add(llEvents);
        linearLayoutArrayList.add(llProfile);
        linearLayoutArrayList.add(llRecurring);
        linearLayoutArrayList.add(llReminder);

        for(int i=0;i<linearLayoutArrayList.size();i++)
        {
            if(linearLayoutArrayList.get(i).equals(linearLayout))
                linearLayoutArrayList.get(i).setBackgroundColor(getResources().getColor(R.color.orangee_light));
            else
                linearLayoutArrayList.get(i).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
    }

    @Override
        public void onClick(View v) {
            if(v.equals(llReminder)) {
                FragmentReminderList fragmentEventList=new FragmentReminderList();
                callFragment(fragmentEventList);
            }else if(v.equals(llProfile)) {
                callFragment(new FragmentProfile());
            }else if(v.equals(llRecurring)) {
                FragmentRecurringList fragmentEventList=new FragmentRecurringList();
                callFragment(fragmentEventList);
            }else if(v.equals(llEvents)) {
                FragmentEventList fragmentEventList=new FragmentEventList();
                callFragment(fragmentEventList);
            }else if(v.equals(llFriends)) {
                callFragment(new FragmentContact());
            }
            setSelected((LinearLayout) v);
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(!TextUtils.isEmpty(new SessionManager(this).getStringValue(SessionManager.USER_ID))) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.main_menu, menu);
            this.menu = menu;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == R.id.menuLogout)
        {
             new SessionManager(this).clearData();
             Toast.makeText(this,"Logout Successfully",Toast.LENGTH_SHORT).show();
             callFragment(new FragmentLogin());
        }
        else if(item.getItemId() == R.id.menuChangePassword)
            callFragment(new FragmentChangePassword());

        return  true;
    }

/*
    public void makeLogout()
    {
        APICall.makeCall(null, Utility.URL_LOGOUT, MainActivity.this, Request.Method.POST, true, true,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object)
                    {
                        try {
                            String result = object.toString();
                            Log.d("Response", result);
                            JSONObject jsonObject = new JSONObject(result);
                            llBottomMenuResident.setVisibility(View.GONE);
                            new SessionManager(MainActivity.this).clearData();
                            Toast.makeText(MainActivity.this,"Logout Successfully",Toast.LENGTH_SHORT).show();
                            invalidateOptionsMenu();
                            callFragment(new FragmentLogin());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Utility.dismissDialog();
                    }

                    @Override
                    public void onError(Object object) {
                    }
                });
    }
*/

    @Override
    public void onBackPressed()
    {
        if(currentFragment instanceof FragmentLogin || currentFragment instanceof FragmentEventList
                || currentFragment instanceof FragmentProfile || currentFragment instanceof FragmentContact)
            backPressToExit();
        else if(currentFragment instanceof FragmentRegister) {
            callFragment(new FragmentLogin());
            toolbar.setNavigationIcon(R.drawable.app_logo_inside);
        }else if(currentFragment instanceof FragmentReminder || currentFragment instanceof FragmentChangePassword)
            callFragment(new FragmentReminderList());
        else if(currentFragment instanceof FragmentRecurringReminder)
            callFragment(new FragmentRecurringList());
        else if(currentFragment instanceof FragmentAddEvent)
            callFragment(new FragmentEventList());
        else if(currentFragment instanceof FragmentAddTask)
            getSupportFragmentManager().popBackStack();
        else
            backPressToExit();
    }

    public void backPressToExit()
    {
        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Snackbar.make(view, "Please click BACK again to exit", Snackbar.LENGTH_SHORT).show();
        //Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        // redirects to utils
        permissionUtils.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }

    @Override
    public void PermissionGranted(int request_code) {
        // startApp();
        Log.i("PERMISSION","GRANTED");
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        Log.i("PERMISSION PARTIALLY","GRANTED");
    }

    @Override
    public void PermissionDenied(int request_code)
    {
        Log.i("PERMISSION","DENIED");
    }

    @Override
    public void NeverAskAgain(int request_code) {
        Log.i("PERMISSION","NEVER ASK AGAIN");
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
        finish();
    }

    @Override
    public void showPermissionMessage()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(Utility.MSG_PERMISSION);
        builder.setTitle("Grant Permissions");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionUtils.check_permission(arrPermissions,1);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

/*
    public void readTextFromRemoteServer()
    {
        new Thread(new Runnable(){

            public void run(){
                final ArrayList<String> urls=new ArrayList<String>(); //to read each line
                //TextView t; //to show the result, please declare and find it inside onCreate()

                try {
                    // Create a URL for the desired page
                    URL url = new URL("https://drive.google.com/open?id=0B2_5Wl2eMHLJSDh4ckZFQVpwRWk2SV8xakhPS043T2U5cFQ4"); //My text file location
                    //First open the connection
                    HttpURLConnection conn=(HttpURLConnection) url.openConnection();
                    conn.setConnectTimeout(60000); // timing out in a minute

                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    //t=(TextView)findViewById(R.id.TextView1); // ideally do this in onCreate()
                    String str;
                    while ((str = in.readLine()) != null) {
                        urls.add(str);
                    }
                    in.close();
                } catch (Exception e) {
                    Log.d("MyTag",e.toString());
                }

                //since we are in background thread, to post results we have to go back to ui thread. do the following for that

                runOnUiThread(new Runnable(){
                    public void run(){
                       Log.d("Data",urls.get(0));
                        //t.setText(urls.get(0)); // My TextFile has 3 lines
                    }
                });

            }
        }).start();
    }
*/

}
