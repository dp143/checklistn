package com.dp.mychecklist.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.util.APICall;
import com.dp.mychecklist.util.GetSharedEvent;
import com.dp.mychecklist.util.GlobalAPIs;
import com.dp.mychecklist.util.SessionManager;
import com.dp.mychecklist.util.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pramodpatil305 on 21-06-2018.
 */

public class FragmentLogin extends Fragment
{
    @BindView(R.id.edtUsername)
    EditText edtUsername;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.tvRegister)
    TextView tvRegister;
    View rootView;

    static boolean isFinish = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_login_new,null);
        ButterKnife.bind(this,rootView);
        isFinish=false;


        ((MainActivity) getActivity()).setTitle("Login");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(validate()) {
                    Utility.hideKeyboard(getActivity(),rootView);
                    try {
                        makeLogin(edtUsername.getText().toString(),edtPassword.getText().toString());
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                  //  presenter.sendDatatoModel(edtUsername.getText().toString(), edtPassword.getText().toString());
                }
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).callFragment(new FragmentRegister());
            }
        });

//        ((MainActivity) getActivity()).setToolbarTitle("Login");
  //      ((MainActivity) getActivity()).toolbar.setNavigationIcon(null);

        ((MainActivity) getActivity()).currentFragment = this;
        return rootView;
    }

    public boolean validate() {
        if(TextUtils.isEmpty(edtUsername.getText().toString()) || TextUtils.isEmpty(edtPassword.getText())) {
            Toast.makeText(getActivity(), "Please enter your mobile number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(edtUsername.getText().toString().length()<10) {
            Toast.makeText(getActivity(), "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void makeLogin(final String mobileNo, final String password)
    {
        Map<String, String> params = new HashMap<>();

        params.put("txtMobileNumber",mobileNo);
        params.put("txtPassword",password);

        APICall.makeCall(params, Utility.URL_LOGIN, getActivity(), Request.Method.POST, false, true,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object)
                    {
                        try {
                            String result = object.toString();
                            Log.d("Response", result);
                            JSONObject jsonObject = new JSONObject(result);
                            if(jsonObject.has("id"))
                            {
                                new SessionManager(getActivity()).setStringValue(SessionManager.USER_ID,jsonObject.getString("id"));
                                new SessionManager(getActivity()).setStringValue(SessionManager.USER_NAME,jsonObject.getString("name"));
                                new SessionManager(getActivity()).setStringValue(SessionManager.USER_EMAIL,jsonObject.getString("email"));
                                new SessionManager(getActivity()).setStringValue(SessionManager.MOBILE_NO,jsonObject.getString("contact_number"));
                                new SessionManager(getActivity()).setStringValue(SessionManager.ACCESS_TOKEN,jsonObject.getString("access_token"));
                                new SessionManager(getActivity()).setStringValue(SessionManager.PASSWORD,password);
                                new GlobalAPIs(getActivity()).getAllEvents(FragmentLogin.this);
                             //   ((MainActivity) getActivity()).startService(new Intent(getActivity(),GetSharedEvent.class));
                            }
                            else
                            {
                                Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                      //  Utility.dismissDialog();
                    }

                    @Override
                    public void onError(Object object)
                    {
                        Log.d("Error",object.toString());
                    }
                });
    }

    public void syncDone()
    {
        Toast.makeText(getActivity(),"Login Successfully",Toast.LENGTH_SHORT).show();
        ((MainActivity) getActivity()).llBottomMenuResident.setVisibility(View.VISIBLE);

        ((MainActivity)getActivity()).startService(new Intent(getActivity(), GetSharedEvent.class));

        ((MainActivity) getActivity()).invalidateOptionsMenu();
        FragmentReminderList fragmentEventList=new FragmentReminderList();
        ((MainActivity)getActivity()).callFragment(fragmentEventList);
    }

}
