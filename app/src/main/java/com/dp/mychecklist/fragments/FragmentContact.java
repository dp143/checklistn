package com.dp.mychecklist.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.adapter.ContactAdapter;
import com.dp.mychecklist.util.ContactModel;
import com.dp.mychecklist.util.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

public class FragmentContact extends Fragment
{
    View rootView;
    SortedMap<String, String> hashMapContacts;
    ArrayList<String> arrNumbers, arrNames;
    ArrayList<ContactModel> arrContacts;
    RecyclerView recyclerView;
    TextView tvInvite;
    EditText edtName;
    Button btnSubmit;
    public String eventCode="";
    ContactAdapter contactAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_contact,null);
        hashMapContacts = Utility.getContactsIntoArrayList(getActivity(),true);
        arrNames = new ArrayList<>();
        arrNumbers = new ArrayList<>();

        tvInvite = (TextView) rootView.findViewById(R.id.tvInvite);
        edtName = (EditText) rootView.findViewById(R.id.edtName);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerContacts);

        for(String strContact:hashMapContacts.keySet())
        {
            Log.d(hashMapContacts.get(strContact),"");
            arrNames.add(strContact);
        }

        edtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                contactAdapter.getFilter().filter((edtName.getText().toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {
                contactAdapter.getFilter().filter((edtName.getText().toString()));
            }
        });

        Collections.sort(arrNames);

        arrContacts = new ArrayList<>();
        for(String strName: arrNames)
        {
            arrNumbers.add(hashMapContacts.get(strName));
            ContactModel contactModel = new ContactModel();
            contactModel.setContactNumber(hashMapContacts.get(strName));
            contactModel.setName(strName);
            arrContacts.add(contactModel);
        }

        if(getArguments() != null)
        {
            if(getArguments().getString("EV") != null)
            {
                eventCode = getArguments().getString("EV");
                Log.d("Cde",eventCode);
            }
        }

        contactAdapter = new ContactAdapter(arrContacts,FragmentContact.this,getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(contactAdapter);

        ((MainActivity) getActivity()).setTitle("Friends");
        ((MainActivity) getActivity()).currentFragment = this;

        tvInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.shareApp(getActivity());
            }
        });

        return rootView;
    }
}
