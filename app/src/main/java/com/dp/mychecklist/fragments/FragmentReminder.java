package com.dp.mychecklist.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.reminders.Reminder;
import com.dp.mychecklist.roomDB.DBThread;
import com.dp.mychecklist.roomDB.entities.TEvent;
import com.dp.mychecklist.util.APICall;
import com.dp.mychecklist.util.Utility;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentReminder extends Fragment
{
    View rootView;
    @BindView(R.id.edtReminderName)
    EditText edtReminderName;
    @BindView(R.id.tvReminderDate)
    TextView tvReminderDate;
    @BindView(R.id.edtReminderDescription)
    EditText edtReminderDescription;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    Calendar myCalendar;
    String strEventDate;
    TEvent tEvent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_reminder, null);

        myCalendar = Calendar.getInstance();

        ButterKnife.bind(this, rootView);
        tvReminderDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTimeDialog();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isVaildate()) {
                    Utility.hideKeyboard(getActivity(),rootView);
                    addEvent();
                }
            }
        });

        ((MainActivity) getActivity()).setTitle("Reminder");


        if (getArguments() != null) {
            if (getArguments().getSerializable("EV") != null) {
                tEvent = (TEvent) getArguments().getSerializable("EV");
                setAllData();
            }
        }
        ((MainActivity) getActivity()).currentFragment = this;

        return rootView;
    }


    public void setAllData()
    {
        edtReminderDescription.setText(tEvent.getEventDescription());
        edtReminderName.setText(tEvent.getEventName());
        tvReminderDate.setText(Utility.getDateInDDMMYYYYHHMMSS(tEvent.getEventDate(),false));
        strEventDate =  tEvent.getEventDate();
        btnSubmit.setText("Update");
    }

    public void showDateTimeDialog() {
        Utility.hideKeyboard(getActivity(), rootView);

        if (!TextUtils.isEmpty(tvReminderDate.getText())) {
            // String[] strDateTime = tvEventDate.getText().toString().split(" ");
            // String[] strDateTime = strEventDate.split(" ");
            strEventDate = Utility.getDateInDDMMYYYYHHMMSS(tvReminderDate.getText().toString(),true);
            int year = Integer.parseInt(strEventDate.substring(0,4));
            int month = Integer.parseInt(strEventDate.substring(4,6));
            int day = Integer.parseInt(strEventDate.substring(6,8));
            int hour = Integer.parseInt(strEventDate.substring(8,10));
            int minutes = Integer.parseInt(strEventDate.substring(10,12));

            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month - 1);
            myCalendar.set(Calendar.DAY_OF_MONTH, day);
            myCalendar.set(Calendar.HOUR_OF_DAY, hour);
            myCalendar.set(Calendar.MINUTE, minutes);
            showDateDialog(hour,minutes);
        } else {
            myCalendar = Calendar.getInstance();

            final int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
            final int minute = myCalendar.get(Calendar.MINUTE);
            showDateDialog(hour,minute);
        }
    }

    public void showDateDialog(final int hour,final int minute)
    {
        new DatePickerDialog(getActivity(),new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, final int year, final int month, final int dayOfMonth)
            {
                final int months = month+1;

                new TimePickerDialog(getActivity(),  new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        //   mFirst = false;
                        String strHour = Utility.getFormatted(hourOfDay);
                        String strMinute = Utility.getFormatted(minute);
                        String strMonth = Utility.getFormatted(months);
                        String strDate = Utility.getFormatted(dayOfMonth);

                        tvReminderDate.setText(Utility.getDateInDDMMYYYYHHMMSS(year+strMonth+strDate+strHour+strMinute+"00",false));
                        strEventDate = year+strMonth+strDate+strHour+strMinute+"00";
                        //setReminderBefore();
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, month);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        myCalendar.set(Calendar.MINUTE, minute);
                        Log.d("Calendar Millis",myCalendar.getTimeInMillis()+"");
                    }
                }, hour, minute, true).show();
            }
        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public boolean isVaildate() {
        if (TextUtils.isEmpty(edtReminderName.getText())) {
            Toast.makeText(getActivity(), "Please enter reminder name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(tvReminderDate.getText())) {
            Toast.makeText(getActivity(), "Please select reminder date", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!Utility.comapreWithTodaysDate(strEventDate))
        {
            Toast.makeText(getActivity(), "Please select valid reminder date", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(edtReminderDescription.getText())) {
            Toast.makeText(getActivity(), "Please enter reminder description", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    Handler handler = new Handler(Looper.getMainLooper())
    {
        @Override
        public void handleMessage(final Message msg) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(msg.what == 1)
                    {
                        if(msg.getData().getLong("insert") > 0)
                            Toast.makeText(getActivity(),"Reminder added successfully",Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(getActivity(),"Unable to add the reminder",Toast.LENGTH_SHORT).show();
                    }
                    else if(msg.what == 3)
                    {
                        if(msg.getData().getLong("insert") > 0)
                            Toast.makeText(getActivity(),"reminder updated successfully",Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(getActivity(),"Unable to update the reminder",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    };

    public void addEvent()
    {
        Map<String, String> params = new HashMap<>();

        params.put("txtEventName",edtReminderName.getText().toString());
        params.put("txtEventDate",strEventDate);
        params.put("txtTaskTypeId","2");
        params.put("txtTaskRepeatDays","0");
        params.put("txtEventDescription",edtReminderDescription.getText().toString());

        if(tEvent != null) {
            params.put("txtEventCode", tEvent.getEventId());
        }

        APICall.makeCall(params, Utility.URL_ADD_EVENT, getActivity(), Request.Method.POST, true, true,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object)
                    {
                        try {
                            String result = object.toString();
                            Log.d("Response", result);
                            JSONObject jsonObject = new JSONObject(result);

                            if (jsonObject.has("id")) {
                                final TEvent tEventLocal = new TEvent();
                                tEventLocal.setEventDate(jsonObject.getString("event_date"));
                                tEventLocal.setEventDescription(jsonObject.getString("event_description"));
                                tEventLocal.setEventName(jsonObject.getString("event_title"));
                                tEventLocal.setEventSt("1");
                                tEventLocal.setTaskTypeId("2");
                                tEventLocal.setEventId(jsonObject.getString("id"));
                                tEventLocal.setEventDT(jsonObject.getString("record_date"));

                                final Reminder reminder = new Reminder();
                                reminder.setStartTime(myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)
                                        ,myCalendar.get(Calendar.HOUR_OF_DAY),myCalendar.get(Calendar.MINUTE));

                                if (tEvent == null) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            long insertId = new DBThread(getActivity()).insertEvent(tEventLocal);
                                            tEvent = tEventLocal;

                                            reminder.settEvent(tEvent);
                                            reminder.setId((int) insertId);
                                            reminder.schedule(getActivity());

                                            Bundle bundle = new Bundle();
                                            bundle.putLong("insert", insertId);
                                            Message message = new Message();
                                            message.what = 1;
                                            message.setData(bundle);
                                            handler.handleMessage(message);
                                        }
                                    }).start();
                                } else {
                                    tEventLocal.setEventRecordId(tEvent.getEventRecordId());
                                    tEvent = tEventLocal;

                                    reminder.setId(tEvent.getEventRecordId());
                                    reminder.settEvent(tEvent);
                                    reminder.schedule(getActivity());

                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            int update = new DBThread(getActivity()).updateEvent(tEvent);
                                            Log.d("update", update + "");
                                            Bundle bundle = new Bundle();
                                            bundle.putLong("insert", update);
                                            Message message = new Message();
                                            message.what = 3;
                                            message.setData(bundle);
                                            handler.handleMessage(message);
                                        }
                                    }).start();
                                }
                                ((MainActivity) getActivity()).onBackPressed();
                            } else {
                                Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Utility.dismissDialog();
                    }

                    @Override
                    public void onError(Object object)
                    {
                    }
                });
    }
}
