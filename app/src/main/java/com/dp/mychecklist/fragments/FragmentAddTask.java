package com.dp.mychecklist.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.reminders.Reminder;
import com.dp.mychecklist.roomDB.DBThread;
import com.dp.mychecklist.roomDB.entities.TEvent;
import com.dp.mychecklist.roomDB.entities.TTask;
import com.dp.mychecklist.util.APICall;
import com.dp.mychecklist.util.Utility;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FragmentAddTask extends Fragment
{
    View rootView;
    @BindView(R.id.edtEventName)
    EditText edtEventName;
    @BindView(R.id.edtTaskName)
    EditText edtTaskName;
    @BindView(R.id.tvTaskDate)
    TextView tvTaskDate;
    @BindView(R.id.edtTaskDescription)
    EditText edtTaskDescription;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    Calendar myCalendar;
    String strTaskDate;
    TTask tTask;
    TEvent tEvent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_add_task_new, null);

        ButterKnife.bind(this, rootView);
        tvTaskDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTimeDialog();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isVaildate()) {
                    Utility.hideKeyboard(getActivity(),rootView);
                    addTask();
                }
            }
        });

        tEvent = (TEvent) getArguments().getSerializable("EV");
        edtEventName.setText(tEvent.getEventName());

        myCalendar =Calendar.getInstance();

        if (getArguments() != null)
        {
            if(getArguments().getSerializable("TS") != null)
            {
                btnSubmit.setText("Update");
                tTask = (TTask) getArguments().getSerializable("TS");
                setAllData();
            }
        }

        ((MainActivity) getActivity()).setTitle("Task");
        ((MainActivity) getActivity()).currentFragment = this;

        return rootView;
    }

    public void setAllData()
    {
        edtTaskName.setText(tTask.getTaskName());
        tvTaskDate.setText(Utility.getDateInDDMMYYYYHHMMSS(tTask.getTaskDate(),false));
        strTaskDate = tTask.getTaskDate();
        edtTaskDescription.setText(tTask.getTaskDescription());
    }

    public void showDateTimeDialog() {
        Utility.hideKeyboard(getActivity(), rootView);

        if (!TextUtils.isEmpty(tvTaskDate.getText())) {
            // String[] strDateTime = tvEventDate.getText().toString().split(" ");
            strTaskDate = Utility.getDateInDDMMYYYYHHMMSS(tvTaskDate.getText().toString(),true);
            int year = Integer.parseInt(strTaskDate.substring(0,4));
            int month = Integer.parseInt(strTaskDate.substring(4,6));
            int day = Integer.parseInt(strTaskDate.substring(6,8));
            int hour = Integer.parseInt(strTaskDate.substring(8,10));
            int minutes = Integer.parseInt(strTaskDate.substring(10,12));

            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month - 1);
            myCalendar.set(Calendar.DAY_OF_MONTH, day);
            myCalendar.set(Calendar.HOUR_OF_DAY, hour);
            myCalendar.set(Calendar.MINUTE, minutes);
            showDateDialog(hour,minutes);
        } else {
            myCalendar = Calendar.getInstance();

            final int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
            final int minute = myCalendar.get(Calendar.MINUTE);
            showDateDialog(hour,minute);
        }
    }

    public void showDateDialog(final int hour,final int minute)
    {
        new DatePickerDialog(getActivity(),new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, final int year, final int month, final int dayOfMonth)
            {
                final int months = month+1;

                new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String strHour = Utility.getFormatted(hourOfDay);
                        String strMinute = Utility.getFormatted(minute);
                        String strMonth = Utility.getFormatted(months);
                        String strDate = Utility.getFormatted(dayOfMonth);

                        tvTaskDate.setText(Utility.getDateInDDMMYYYYHHMMSS(year+strMonth+strDate+strHour+strMinute+"00",false));
                        strTaskDate = year+strMonth+strDate+strHour+strMinute+"00";
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, month);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        myCalendar.set(Calendar.MINUTE, minute);
                        Log.d("Calendar Millis",myCalendar.getTimeInMillis()+"");
                    }
                }, hour, minute, true).show();
            }
        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public boolean isVaildate() {
        if (TextUtils.isEmpty(edtTaskName.getText())) {
            Toast.makeText(getActivity(), "Please enter task name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(tvTaskDate.getText())) {
            Toast.makeText(getActivity(), "Please select task date", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!Utility.comapreWithTodaysDate(strTaskDate))
        {
            Toast.makeText(getActivity(), "Please select valid task date", Toast.LENGTH_SHORT).show();
            return false;
        }
       else if (TextUtils.isEmpty(edtTaskDescription.getText())) {
            Toast.makeText(getActivity(), "Please enter task description", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    Handler handler = new Handler(Looper.getMainLooper())
    {
        @Override
        public void handleMessage(final Message msg) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(msg.what == 1)
                    {
                        if(msg.getData().getLong("insert") > 0)
                            Toast.makeText(getActivity(),"Task added successfully",Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(getActivity(),"Unable to add the task",Toast.LENGTH_SHORT).show();
                    }
                    else if(msg.what == 2)
                    {
                        if(msg.getData().getLong("insert") > 0)
                            Toast.makeText(getActivity(),"Task updated successfully",Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(getActivity(),"Unable to update the task",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    };

    public void addTask()
    {
        Map<String, String> params = new HashMap<>();

        params.put("txtTaskName",edtTaskName.getText().toString());
        params.put("txtTaskDate",strTaskDate);
        params.put("txtTaskDescription",edtTaskDescription.getText().toString());
        params.put("txtEventCode",tEvent.getEventId());

        if(tTask != null) {
            params.put("txtTaskCode", tTask.getTaskId());
        }

        APICall.makeCall(params, Utility.URL_ADD_TASK, getActivity(), Request.Method.POST, true, true,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object)
                    {
                        try {
                            String result = object.toString();
                            Log.d("Response", result);
                            final JSONObject jsonObject = new JSONObject(result);

                            if (jsonObject.has("id")) {
                                final TTask tTaskLocal = new TTask();
                                tTaskLocal.setTaskDate(strTaskDate);
                                tTaskLocal.setTaskDescription(edtTaskDescription.getText().toString());
                                tTaskLocal.setTaskName(edtTaskName.getText().toString());
                                tTaskLocal.setTaskSt("1");

                                final Reminder reminder = new Reminder();
                                reminder.setStartTime(myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)
                                        ,myCalendar.get(Calendar.HOUR_OF_DAY),myCalendar.get(Calendar.MINUTE));

                                if(tTask == null) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                tTaskLocal.setTaskId(jsonObject.getString("id"));
                                                tTaskLocal.setFk_eventId(tEvent.getEventId());
                                                tTaskLocal.setTaskDT(Utility.getCurrentDate());
                                                long insertId = new DBThread(getActivity()).insertTask(tTaskLocal);

                                                reminder.settTask(tTaskLocal);
                                                reminder.setId((int) insertId);
                                                reminder.schedule(getActivity());

                                                Bundle bundle = new Bundle();
                                                bundle.putLong("insert", insertId);
                                                Message message = new Message();
                                                message.what = 1;
                                                message.setData(bundle);
                                                handler.handleMessage(message);
                                            }
                                            catch (Exception e)
                                            {
                                                e.printStackTrace();
                                            }
                                        }
                                    }).start();
                                }
                                else
                                {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            tTaskLocal.setTaskRecordId(tTask.getTaskRecordId());
                                            tTaskLocal.setFk_eventId(tTask.getFk_eventId());
                                            tTaskLocal.setTaskId(tTask.getTaskId());
                                            tTask=tTaskLocal;

                                            reminder.settTask(tTaskLocal);
                                            reminder.setId((int) tTask.getTaskRecordId());
                                            reminder.schedule(getActivity());

                                            int update = new DBThread(getActivity()).updateTask(tTask);
                                            Log.d("update",update+"");
                                            Bundle bundle=new Bundle();
                                            bundle.putLong("insert",update);
                                            Message message= new Message();
                                            message.what = 2;
                                            message.setData(bundle);
                                            handler.handleMessage(message);
                                        }
                                    }).start();
                                }
                                ((MainActivity) getActivity()).onBackPressed();
                            } else {
                                Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Utility.dismissDialog();
                    }

                    @Override
                    public void onError(Object object)
                    {
                    }
                });
    }
}
