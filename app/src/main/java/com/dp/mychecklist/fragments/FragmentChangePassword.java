package com.dp.mychecklist.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.util.APICall;
import com.dp.mychecklist.util.SessionManager;
import com.dp.mychecklist.util.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pramodpatil305 on 21-06-2018.
 */

public class FragmentChangePassword extends Fragment
{
    @BindView(R.id.edtNewPassword)
    EditText edtNewPassword;
    @BindView(R.id.edtCurrent)
    EditText edtCurrent;
    @BindView(R.id.edtConfirmPass)
    EditText edtConfirmPass;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    View rootView;
    static boolean isFinish = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_change_password, null);
        ButterKnife.bind(this, rootView);
        isFinish = false;

        ((MainActivity) getActivity()).setTitle("Change Password");

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    Utility.hideKeyboard(getActivity(), rootView);
                    try {
                        updateProfile();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        ((MainActivity) getActivity()).currentFragment = this;
        return rootView;
    }

    public boolean validate() {
        if(TextUtils.isEmpty(edtCurrent.getText().toString())) {
            Toast.makeText(getActivity(), "Please enter current password", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(edtNewPassword.getText().toString())) {
            Toast.makeText(getActivity(), "Please enter new password", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(edtConfirmPass.getText().toString())) {
            Toast.makeText(getActivity(), "Please enter confirmed password", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!edtCurrent.getText().toString().equals(new SessionManager(getActivity()).getStringValue(SessionManager.PASSWORD)))
        {
            Toast.makeText(getActivity(), "Please enter enter the valid current password", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!Utility.isValidPassword(edtNewPassword.getText().toString()))
        {
            Toast.makeText(getActivity(), "Password must have atleast 8 characters which contains atleast one number and one character", Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!edtNewPassword.getText().toString().equals(edtConfirmPass.getText().toString())) {
            Toast.makeText(getActivity(), "New and confirmed password must be same", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void updateProfile()
    {
        Map<String, String> params = new HashMap<>();
        params.put("txtCurrentPassword",edtCurrent.getText().toString());
        params.put("txtNewPassword",edtNewPassword.getText().toString());
        params.put("txtConfirmPassword",edtConfirmPass.getText().toString());
        params.put("txtContactNumber",new SessionManager(getActivity()).getStringValue(SessionManager.MOBILE_NO));

        APICall.makeCall(params, Utility.URL_CHANGE_PASSWORD, getActivity(), Request.Method.POST, true, true,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object)
                    {
                        try {
                            String result = object.toString();
                            Log.d("Response", result);
                            JSONObject jsonObject = new JSONObject(result);
                            Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                            new SessionManager(getActivity()).setStringValue(SessionManager.PASSWORD,edtNewPassword.getText().toString());

                            ((MainActivity) getActivity()).onBackPressed();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Utility.dismissDialog();
                    }

                    @Override
                    public void onError(Object object) {
                    }
                });
    }

}
