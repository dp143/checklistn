package com.dp.mychecklist.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.util.APICall;
import com.dp.mychecklist.util.SessionManager;
import com.dp.mychecklist.util.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pramodpatil305 on 21-06-2018.
 */

public class FragmentProfile extends Fragment
{
    @BindView(R.id.edtEmailId)
    EditText edtEmailId;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtMobileNo)
    EditText edtMobile;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.tvPassword)
    TextView tvPassword;
    View rootView;
    static boolean isFinish = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_register_new, null);
        ButterKnife.bind(this, rootView);
        isFinish = false;

        ((MainActivity) getActivity()).setTitle("Profile");
        edtName.setText(new SessionManager(getActivity()).getStringValue(SessionManager.USER_NAME));
        edtEmailId.setText(new SessionManager(getActivity()).getStringValue(SessionManager.USER_EMAIL));
        edtMobile.setText(new SessionManager(getActivity()).getStringValue(SessionManager.MOBILE_NO));

        edtEmailId.setEnabled(false);
        edtName.setEnabled(false);
        edtMobile.setEnabled(false);

        btnSubmit.setText("Update");
        btnSubmit.setVisibility(View.GONE);

        tvPassword.setVisibility(View.GONE);
        edtPassword.setVisibility(View.GONE);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    Utility.hideKeyboard(getActivity(), rootView);
                    try {
                        updateProfile();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        ((MainActivity) getActivity()).currentFragment = this;
        return rootView;
    }

    public boolean validate() {
        if(TextUtils.isEmpty(edtName.getText().toString()) || TextUtils.isEmpty(edtName.getText())) {
            Toast.makeText(getActivity(), "Please enter your name", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(edtMobile.getText().toString()) || TextUtils.isEmpty(edtMobile.getText())) {
            Toast.makeText(getActivity(), "Please enter your mobile number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(edtMobile.getText().toString().length()<10) {
            Toast.makeText(getActivity(), "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(edtEmailId.getText().toString())) {
            Toast.makeText(getActivity(), "Please enter email-Id", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void updateProfile()
    {
        Map<String, String> params = new HashMap<>();

        params.put("txtName",edtName.getText().toString());
        params.put("txtMobileNumber",edtMobile.getText().toString());
        params.put("txtEmail",edtEmailId.getText().toString());

        APICall.makeCall(params, Utility.URL_REGISTER, getActivity(), Request.Method.POST, false, true,
                new APICall.APIListener() {
                    @Override
                    public void onSuccess(Object object)
                    {
                        try {
                            String result = object.toString();
                            Log.d("Response", result);
                            JSONObject jsonObject = new JSONObject(result);

                            if(jsonObject.has("id"))
                            {
                                new SessionManager(getActivity()).setStringValue(SessionManager.USER_NAME,jsonObject.getString("name"));
                                new SessionManager(getActivity()).setStringValue(SessionManager.USER_EMAIL,jsonObject.getString("email"));
                                new SessionManager(getActivity()).setStringValue(SessionManager.MOBILE_NO,jsonObject.getString("contact_number"));
                                new SessionManager(getActivity()).setStringValue(SessionManager.ACCESS_TOKEN,jsonObject.getString("access_token"));

                                Toast.makeText(getActivity(),"User Registered Successfully",Toast.LENGTH_SHORT).show();
                                ((MainActivity) getActivity()).menu.getItem(0).setVisible(true);
                                ((MainActivity) getActivity()).llBottomMenuResident.setVisibility(View.VISIBLE);

                                Bundle bundle= new Bundle();
                                bundle.putString("type","2");
                                FragmentEventList fragmentEventList=new FragmentEventList();
                                fragmentEventList.setArguments(bundle);
                                ((MainActivity)getActivity()).callFragment(fragmentEventList);
                            }
                            else
                            {
                                Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Utility.dismissDialog();
                    }

                    @Override
                    public void onError(Object object) {
                    }
                });
    }

}
