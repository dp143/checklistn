package com.dp.mychecklist.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dp.mychecklist.MainActivity;
import com.dp.mychecklist.R;
import com.dp.mychecklist.adapter.EventAdapter;
import com.dp.mychecklist.roomDB.DBThread;
import com.dp.mychecklist.roomDB.entities.TEvent;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentRecurringList extends Fragment
{
    View rootView;
    @BindView(R.id.recyclerEvents)
    RecyclerView recyclerView;
    @BindView(R.id.tvNoEvents)
    TextView tvNoData;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    ArrayList<TEvent> eventArrayList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.event_list, null);

        ButterKnife.bind(this, rootView);

        eventArrayList = new ArrayList<>();

        ((MainActivity) getActivity()).setTitle("Recurring Tasks");

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).callFragment(new FragmentRecurringReminder());
            }
        });

        //  eventModelArrayList = ((MainActivity) getActivity()).dao.getAllEvents();
        ((MainActivity) getActivity()).currentFragment = this;
        getMyEventList();
        return rootView;
    }

    public void getMyEventList()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String type = "3";

                eventArrayList = new DBThread(getActivity()).getEvents(type);
                Message message = new Message();
                message.what = 1;
                handler.handleMessage(message);
            }
        }).start();
    }

    Handler handler = new Handler(Looper.getMainLooper())
    {
        @Override
        public void handleMessage(final Message msg) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(msg.what == 1)
                    {
                        if(eventArrayList.size()>0)
                        {
                            EventAdapter eventAdapter = new EventAdapter(eventArrayList,FragmentRecurringList.this,getActivity(),recyclerView);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
                            recyclerView.setAdapter(eventAdapter);
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setVisibility(View.VISIBLE);
                            tvNoData.setVisibility(View.GONE);
                        }
                        else
                        {
                            recyclerView.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });
        }
    };
}
